//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jul 25 06:23:42 2017 by ROOT version 6.08/06
// from TTree cbmsim//cbmroot
// found on file: spectrohits_EPFL.root
//////////////////////////////////////////////////////////

#ifndef ShipClass_h
#define ShipClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <stdio.h>
#include <TTree.h>
// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TObject.h"
#include <iostream>
#include <vector>

#define kMaxcbmroot_Stack_MCTrack  205931
#define kMaxcbmroot_Spectrometer_SpectrometerPoint 3010

class ShipClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

//Relevant variables to save in the tree
   Int_t Event; //event number
   std::vector<double> x; //x position of the hit
   std::vector<double> y; //y position of the hit
   std::vector<double> z; //z position of the hit
   std::vector<double> thetaangle; //azimuth angle (y reference axis)
   std::vector<double> phiangle; //angle in the x,z plane
   std::vector<double> Ptot; //total momentum of the particle
   std::vector<double> PdgID; //abs ID of the particle
   std::vector<double> ch; //abs ID of the particle
   std::vector<std::vector<double>> L1_S1_N;
   std::vector<std::vector<double>> L1_S1_S;
   std::vector<std::vector<double>> L1_S1_D;
   std::vector<std::vector<double>> L1_S1_P;

    
// Fixed size dimensions of array or collections stored in the TTree if any.
//   const Int_t kMaxcbmroot_Stack_MCTrack = 205931;
//   const Int_t kMaxcbmroot_Spectrometer_SpectrometerPoint = 3010;

   // Declaration of leaf types
   Int_t           MCTrack_;
   UInt_t          MCTrack_fUniqueID[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   UInt_t          MCTrack_fBits[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Int_t           MCTrack_fPdgCode[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Int_t           MCTrack_fMotherId[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fPx[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fPy[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fPz[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fM[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fStartX[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fStartY[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fStartZ[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fStartT[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Double32_t      MCTrack_fW[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Int_t           MCTrack_fNPoints[kMaxcbmroot_Stack_MCTrack];   //[cbmroot.Stack.MCTrack_]
   Int_t           SpectrometerPoint_;
   UInt_t          SpectrometerPoint_fUniqueID[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   UInt_t          SpectrometerPoint_fBits[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Int_t           SpectrometerPoint_fTrackID[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   UInt_t          SpectrometerPoint_fEventId[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fPx[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fPy[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fPz[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fTime[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fLength[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fELoss[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Int_t           SpectrometerPoint_fDetectorID[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fX[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fY[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Double32_t      SpectrometerPoint_fZ[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]
   Int_t           SpectrometerPoint_fPdgCode[kMaxcbmroot_Spectrometer_SpectrometerPoint];   //[cbmroot.Spectrometer.SpectrometerPoint_]

   // List of branches
   TBranch        *b_cbmroot_Stack_MCTrack_;   //!
   TBranch        *b_MCTrack_fUniqueID;   //!
   TBranch        *b_MCTrack_fBits;   //!
   TBranch        *b_MCTrack_fPdgCode;   //!
   TBranch        *b_MCTrack_fMotherId;   //!
   TBranch        *b_MCTrack_fPx;   //!
   TBranch        *b_MCTrack_fPy;   //!
   TBranch        *b_MCTrack_fPz;   //!
   TBranch        *b_MCTrack_fM;   //!
   TBranch        *b_MCTrack_fStartX;   //!
   TBranch        *b_MCTrack_fStartY;   //!
   TBranch        *b_MCTrack_fStartZ;   //!
   TBranch        *b_MCTrack_fStartT;   //!
   TBranch        *b_MCTrack_fW;   //!
   TBranch        *b_MCTrack_fNPoints;   //!
   TBranch        *b_cbmroot_Spectrometer_SpectrometerPoint_;   //!
   TBranch        *b_SpectrometerPoint_fUniqueID;   //!
   TBranch        *b_SpectrometerPoint_fBits;   //!
   TBranch        *b_SpectrometerPoint_fTrackID;   //!
   TBranch        *b_SpectrometerPoint_fEventId;   //!
   TBranch        *b_SpectrometerPoint_fPx;   //!
   TBranch        *b_SpectrometerPoint_fPy;   //!
   TBranch        *b_SpectrometerPoint_fPz;   //!
   TBranch        *b_SpectrometerPoint_fTime;   //!
   TBranch        *b_SpectrometerPoint_fLength;   //!
   TBranch        *b_SpectrometerPoint_fELoss;   //!
   TBranch        *b_SpectrometerPoint_fDetectorID;   //!
   TBranch        *b_SpectrometerPoint_fX;   //!
   TBranch        *b_SpectrometerPoint_fY;   //!
   TBranch        *b_SpectrometerPoint_fZ;   //!
   TBranch        *b_SpectrometerPoint_fPdgCode;   //!

   ShipClass(TTree *tree=0);
   virtual ~ShipClass();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(string station);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
    
};

#endif

#ifdef ShipClass_cxx
ShipClass::ShipClass(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("spectrohits_EPFL.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("spectrohits_EPFL.root");
      }
      f->GetObject("cbmsim",tree);

   }
   Init(tree);
}

ShipClass::~ShipClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ShipClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ShipClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ShipClass::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("MCTrack", &MCTrack_, &b_cbmroot_Stack_MCTrack_);
   fChain->SetBranchAddress("MCTrack.fUniqueID", MCTrack_fUniqueID, &b_MCTrack_fUniqueID);
   fChain->SetBranchAddress("MCTrack.fBits", MCTrack_fBits, &b_MCTrack_fBits);
   fChain->SetBranchAddress("MCTrack.fPdgCode", MCTrack_fPdgCode, &b_MCTrack_fPdgCode);
   fChain->SetBranchAddress("MCTrack.fMotherId", MCTrack_fMotherId, &b_MCTrack_fMotherId);
   fChain->SetBranchAddress("MCTrack.fPx", MCTrack_fPx, &b_MCTrack_fPx);
   fChain->SetBranchAddress("MCTrack.fPy", MCTrack_fPy, &b_MCTrack_fPy);
   fChain->SetBranchAddress("MCTrack.fPz", MCTrack_fPz, &b_MCTrack_fPz);
   fChain->SetBranchAddress("MCTrack.fM", MCTrack_fM, &b_MCTrack_fM);
   fChain->SetBranchAddress("MCTrack.fStartX", MCTrack_fStartX, &b_MCTrack_fStartX);
   fChain->SetBranchAddress("MCTrack.fStartY", MCTrack_fStartY, &b_MCTrack_fStartY);
   fChain->SetBranchAddress("MCTrack.fStartZ", MCTrack_fStartZ, &b_MCTrack_fStartZ);
   fChain->SetBranchAddress("MCTrack.fStartT", MCTrack_fStartT, &b_MCTrack_fStartT);
   fChain->SetBranchAddress("MCTrack.fW", MCTrack_fW, &b_MCTrack_fW);
   fChain->SetBranchAddress("MCTrack.fNPoints", MCTrack_fNPoints, &b_MCTrack_fNPoints);
   fChain->SetBranchAddress("SpectrometerPoint", &SpectrometerPoint_, &b_cbmroot_Spectrometer_SpectrometerPoint_);
   fChain->SetBranchAddress("SpectrometerPoint.fUniqueID", SpectrometerPoint_fUniqueID, &b_SpectrometerPoint_fUniqueID);
   fChain->SetBranchAddress("SpectrometerPoint.fBits", SpectrometerPoint_fBits, &b_SpectrometerPoint_fBits);
   fChain->SetBranchAddress("SpectrometerPoint.fTrackID", SpectrometerPoint_fTrackID, &b_SpectrometerPoint_fTrackID);
   fChain->SetBranchAddress("SpectrometerPoint.fEventId", SpectrometerPoint_fEventId, &b_SpectrometerPoint_fEventId);
   fChain->SetBranchAddress("SpectrometerPoint.fPx", SpectrometerPoint_fPx, &b_SpectrometerPoint_fPx);
   fChain->SetBranchAddress("SpectrometerPoint.fPy", SpectrometerPoint_fPy, &b_SpectrometerPoint_fPy);
   fChain->SetBranchAddress("SpectrometerPoint.fPz", SpectrometerPoint_fPz, &b_SpectrometerPoint_fPz);
   fChain->SetBranchAddress("SpectrometerPoint.fTime", SpectrometerPoint_fTime, &b_SpectrometerPoint_fTime);
   fChain->SetBranchAddress("SpectrometerPoint.fLength", SpectrometerPoint_fLength, &b_SpectrometerPoint_fLength);
   fChain->SetBranchAddress("SpectrometerPoint.fELoss", SpectrometerPoint_fELoss, &b_SpectrometerPoint_fELoss);
   fChain->SetBranchAddress("SpectrometerPoint.fDetectorID", SpectrometerPoint_fDetectorID, &b_SpectrometerPoint_fDetectorID);
   fChain->SetBranchAddress("SpectrometerPoint.fX", SpectrometerPoint_fX, &b_SpectrometerPoint_fX);
   fChain->SetBranchAddress("SpectrometerPoint.fY", SpectrometerPoint_fY, &b_SpectrometerPoint_fY);
   fChain->SetBranchAddress("SpectrometerPoint.fZ", SpectrometerPoint_fZ, &b_SpectrometerPoint_fZ);
   fChain->SetBranchAddress("SpectrometerPoint.fPdgCode", SpectrometerPoint_fPdgCode, &b_SpectrometerPoint_fPdgCode);
   Notify();
}

Bool_t ShipClass::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ShipClass::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ShipClass::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ShipClass_cxx
