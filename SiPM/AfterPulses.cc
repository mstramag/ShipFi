/*
 * AfterPulses.cc
 *
 *  Created on: Dec 7, 2012
 *      Author: bruggiss
 */

#include "AfterPulses.h"
#include <cstdlib>
#include <cmath>

AfterPulses::AfterPulses(){}

void AfterPulses::generateAfterPulses(double const& t0, double const& recoveryT,double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT) {
    /*n_ap(dt) = N_apf/tau_apf*exp(-dt/tau_apf)+N_aps/tau_aps*exp(-dt/tau_aps)
     *n_ap(dt): the probability density for after pulses
     *tau_apf and tau_aps: one describes a fast component of after pulse generation and the other a slow one.
     * */
    double Max=RAND_MAX;
    double r(0.0); //needed to evaluate whether or not an after pulse is created
    double u(0.0); //for the probability distribution of the time
    double x(0.0); //for the probability distribution of the time
    do{
        r=rand()/Max;
        u=rand()/Max;
        x=rand()/Max*maxT; //as the formula below is not really a probability density we have to stop at a certain value because otherwise it will not be itegrateable.
        while(u>(weightSl*exp(-x/tauSl)+(1.0-weightSl)*exp(-x/tauFa))){ //this probability density is given in the "Characterisation Studies of Silicon Photomultipliers" by Patrick Eckert    et al. arXiv:1003.6071v2
            u=rand()/Max;                                                 //the time is calculated using the rejection method.
            x=rand()/Max*maxT;
        }
        time.push_back(x+t0);
        intensity.push_back(1-exp(-x/recoveryT));    //the intensity of the after pulse depends on the state of the pixel that is recharging with a time constant recoveryT
    }while(r<prob);
}


vector<double> AfterPulses::getTime() const {
    return time;
}

vector<double> AfterPulses::getIntensity() const {
    return intensity;
}
