/*
 * SignalSetup.cc
 *
 *  Created on: Jan 21, 2013
 *      Author: bruggiss
 */

#include "SignalSetup.h"
#include <ctime>
#include <cmath>
#include <iostream>
#include "TCanvas.h"
 #include <fstream>
 #include <string>
 #include <map>
 #include <random>
 #include <chrono>


SignalSetup::SignalSetup(double const& diam, double const& diamSigma, double const& gap, double const& posSigma, double const& channelWidth,
  double const& channelHight,double const& epoxy, TRandom3* rand, unsigned int const& numbOfDistr, double const& QuantEff, unsigned int const& layers,unsigned int const& nfibers, double const& trapped, double const& reflectivity, double const& attenuationlength) :
  fiber_diameter(diam),fiber_gap(gap),sig_channels(new SignalChannels(channelWidth,channelHight,epoxy,rand,numbOfDistr,QuantEff)),TParticle(new TLine()),
  random(new TRandom3(time(NULL))),layers_(layers),nfibers_(nfibers), trapped(trapped),reflectivity(reflectivity),attenuationlength(attenuationlength) {
//std::cout<<QuantEff<<std::endl; 
  //erase times 0.0 in order not to have a perfect alignement of the fibers and the channels.  
  double x=-random->Rndm()*fiber_diameter/2.0; 
  double y=sqrt(pow(fiber_diameter,2.0)-pow((fiber_diameter+fiber_gap)/2.0,2.0));
  double y_=fiber_diameter/2*(cos(asin((fiber_diameter+fiber_gap)/2.0/fiber_diameter)));  

 // Constructing the fiber lattice: 128 fibers times i=3,4,5,6 layers... 
  for(unsigned int k=0;k<1600;k++){     
  TChannels.push_back(new TBox(k*channelWidth,-channelHight/2.0,(k+1.0)*channelWidth,channelHight/2.0));

  // We draw fibers with the two shifted patterns
  if(layers==3) {
    // For each position along the 1600 fibers, the fiber is created calling the SignalFiber. 
    for(unsigned int i=0;i<2;i++) { 
      // For the even number layers, the fibers are stacked with the usual x coordinate.      
      fibers[i*2][k]=new SignalFiber(x, (i-1.0)*y+i*y, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);      
      TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        if(i*2+1<3) {
          // For odd number layers, the x position is shifted by  1/2(d+gap)
          fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, i*y, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);   
          TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
          
        }
      }
   }

   //Define the geometry for a 4 layer mats
  if (layers==4) {
    for(unsigned int i=0;i<2;i++) {
    
      if(i==0){
        fibers[i*2][k]=new SignalFiber(x, -1.0*y-1.0*y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, -1.0*y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }
           
      else if (i==1){
        fibers[i*2][k]=new SignalFiber(x, y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, y+y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }
    }
  }



  //Define the geometry for a 5 layer mats
  if(layers==5) {
    for(unsigned int i=0;i<3;i++) {
      fibers[i*2][k]=new SignalFiber(x, 2.0*(i-1.0)*y, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
      TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
      
      if(i*2+1<5) {
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, (i*2.0-1.0)*y, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }
    }
  }


  //Define the geometry for a 6 layer mats    
  if (layers==6) {
    for(unsigned int i=0;i<3;i++) {
      if(i==0){
        fibers[i*2][k]=new SignalFiber(x, -2.0*y-1.0*y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, -1.0*y-1.0*y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }
           
      else if (i==1){
        fibers[i*2][k]=new SignalFiber(x, -y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, +y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }

      else if(i==2) {
        fibers[i*2][k]=new SignalFiber(x, y+1.0*y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2][k]->getX(),fibers[i*2][k]->getY(),fibers[i*2][k]->getDiameter()/2.0));
        fibers[i*2+1][k]=new SignalFiber(x+(fiber_diameter+fiber_gap)/2.0, 2.0*y+y_, posSigma, fiber_gap/2.0, fiber_diameter, diamSigma,random);
        TFibers.push_back(new TEllipse(fibers[i*2+1][k]->getX(),fibers[i*2+1][k]->getY(),fibers[i*2+1][k]->getDiameter()/2.0));
      }
    }
  }
  x+=fiber_diameter+fiber_gap;
  }

}

//Function that simulates the Particle -> Generation in fibre -> Create photons in fibre -> Check cross-talk between fibre -> Fill pixel -> Fill Channel

void SignalSetup::SimulateParticle(double const& theta, double const& phi, double const& x0, double const& meanPhotons, double const& sigma,
				   double const& probFiberxtalk, unsigned int const& method, unsigned int const& layers, unsigned int const& nfibers,double const& fiber_length, double const& fxt,double const& dfxt, double const& angle_cut) {

  double excitation_point=fiber_length*random->Rndm();

  //sig_channels->InitializePhotonsAndPixels();
  //Create photons in the fibers
  for(unsigned int i=0;i<layers_;i++) {
    for(unsigned int j=0;j<nfibers_;j++) {
      fibers[i][j]->createPhotons(theta,phi,x0,meanPhotons, sigma,excitation_point,fiber_length, trapped, reflectivity, attenuationlength, fxt);
    }
  }

  //Check the crosstalk -> Each photon has a probability following a Poisson distribution to 
  // passe to a direct neighbooring fiber.
  // The mean of the Poisson distribution is given as input in the config file.

  for(unsigned int i=0;i<layers_;i++) {
    for(unsigned int j=0;j<nfibers_;j++) {
      //Seed base on time of the computer. Seed for the touching neighboors on the diagonal
      unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
      std::default_random_engine generator (seed);
      //Seed for the neighboor on the same layer
      unsigned seed_direct = std::chrono::system_clock::now().time_since_epoch().count();
      std::default_random_engine generator_direct (seed_direct);

      unsigned int UV=fibers[i][j]->getNumbUVPhotons();
//divide the UV photons number by 4 to split them in the four neighbour fibers (assume the probability in the fur directions to be the same)
      UV=UV/4;
      if(UV!=0){ std::poisson_distribution<int> distribution (UV*fxt);
	std::poisson_distribution<int> distribution_direct (UV*dfxt);
        
      // Case number of layer = 5
      if(layers==4){

//the 0.10 represents the 10% fraction of photons catched inside the neighbour fibre, the others get lost

        //First fibre from first layer row 0
        if((i == 0) && (j == 0)){
	  //Direct
	  fibers[0][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1);}

	if((i==0) && (j == nfibers_-1)){
	  //Direct
	  fibers[0][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);}


	//Row 1  
	
	if((i == 1) && (j == 0)){
	  //Diagonal	 
	  fibers[0][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][1]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
	
	if((i == 1) && (j == nfibers_-1)){
	  //Diagonal
	  fibers[0][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1); 
	  fibers[0][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
  
	//Row 2

        if((i == 2) && (j == 0)){
	  //Direct
	  fibers[2][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1); }

	if((i == 2) && (j == nfibers_-1)){
	  //Direct
	  fibers[2][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);}

	//Row 3

	if((i == 3) && (j == 0)){
	  //Direct
	  fibers[3][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);}
	if((i == 3) && (j == nfibers_-1)){
	  //Direct
	  fibers[3][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);}



        //Bottom layer
        if((i==0) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[0][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);}
              
        //Top layer
        if((i==3) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[3][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[3][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[2][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);}
          

        //Layers in the middle
        if((i==1) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[1][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[1][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[0][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution(generator)*0.1);}

        if((i==2) && (j !=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[2][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j]->addManyPhoton(distribution(generator)*0.1);}

      }




      // Case number of layer = 5
      if(layers==5){

//the 0.10 represents the 10% fraction of photons catched inside the neighbour fibre, the others get lost

        //First fibre from first layer row 0
        if((i == 0) && (j == 0)){
	  //Direct
	  fibers[0][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1);}

	if((i==0) && (j == nfibers_-1)){
	  //Direct
	  fibers[0][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);}


	//Row 1  
	
	if((i == 1) && (j == 0)){
	  //Diagonal	 
	  fibers[0][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][1]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
	
	if((i == 1) && (j == nfibers_-1)){
	  //Diagonal
	  fibers[0][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1); 
	  fibers[0][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
  
	//Row 2

        if((i == 2) && (j == 0)){
	  //Direct
	  fibers[2][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1); }

	if((i == 2) && (j == nfibers_-1)){
	  //Direct
	  fibers[2][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);}


	//Row 3

	if((i == 3) && (j == 0)){
	  //Direct
	  fibers[3][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[4][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1); 
	  fibers[4][1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);}
       
        if((i == 3) && (j == nfibers_-1)){
	  //Diagonal
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][nfibers_-1]->addManyPhoton(distribution(generator)*0.1); 	 
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[3][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);}
              
	//Row 4

	if((i == 4) && (j == 0)){
	  //Direct
	  fibers[4][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][0]->addManyPhoton(distribution(generator)*0.1);}
	if((i == 4) && (j == nfibers_-1)){
	  //Direct
	  fibers[4][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);}
	  



        //Bottom layer
        if((i==0) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[0][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);}
              
        //Top layer
        if((i==4) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[4][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[4][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j]->addManyPhoton(distribution(generator)*0.1);}
          

        //Layers in the middle
        if((i==1) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[1][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[1][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[0][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution(generator)*0.1);}

        if((i==2) && (j !=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[2][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j]->addManyPhoton(distribution(generator)*0.1);}

        if((i==3) && (j!=0) && (j !=nfibers_-1)){
	  //Direct
	  fibers[3][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[3][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][j+1]->addManyPhoton(distribution(generator)*0.1);}
      }


      //********************************

      // Case of a 6 layer detector

      //********************************


      if(layers==6){        
	
	//First fibre from first layer row 0
       
	if((i == 0) && (j == 0)){
	  //Direct
	  fibers[0][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1);}

	if((i==0) && (j == nfibers_-1)){
	  //Direct
	  fibers[0][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);}


	//Row 1  
	
	if((i == 1) && (j == 0)){
	  //Diagonal	 
	  fibers[0][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][1]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
	
	if((i == 1) && (j == nfibers_-1)){
	  //Diagonal
	  fibers[0][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1); 
	  fibers[0][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[1][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1); }
  
	//Row 2

        if((i == 2) && (j == 0)){
	  //Direct
	  fibers[2][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][0]->addManyPhoton(distribution(generator)*0.1); }

	if((i == 2) && (j == nfibers_-1)){
	  //Direct
	  fibers[2][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);}


	//Row 3

	if((i == 3) && (j == 0)){
	  //Direct
	  fibers[3][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[4][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][0]->addManyPhoton(distribution(generator)*0.1); 
	  fibers[4][1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][1]->addManyPhoton(distribution(generator)*0.1);}
       
        if((i == 3) && (j == nfibers_-1)){
	  //Diagonal
	  fibers[2][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][nfibers_-1]->addManyPhoton(distribution(generator)*0.1); 	 
	  fibers[2][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);
	  //Direct
	  fibers[3][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);}
              
	//Row 4

	if((i == 4) && (j == 0)){
	  //Direct
	  fibers[4][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[5][0]->addManyPhoton(distribution(generator)*0.1);}
	if((i == 4) && (j == nfibers_-1)){
	  //Direct
	  fibers[4][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[5][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);}
	  

	//Row 5

	if((i == 5) && (j == 0)){
	  //Direct
	  fibers[5][1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[4][0]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][1]->addManyPhoton(distribution(generator)*0.1);}
	if((i == 5) && (j == nfibers_-1)){
	  //Direct
	  fibers[5][nfibers_-2]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[4][nfibers_-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][nfibers_-2]->addManyPhoton(distribution(generator)*0.1);}


        //Bottom layer
        if((i==0) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[0][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);}
              
        //Top layer
        if((i==5) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[5][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[5][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[4][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][j]->addManyPhoton(distribution(generator)*0.1);}
          

        //Layers in the middle
        if((i==1) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[1][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[1][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[0][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[0][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution(generator)*0.1);}

        if((i==2) && (j!=0) && (j!=nfibers_-1)){
	  //Direct
	  fibers[2][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[1][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[1][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j-1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j]->addManyPhoton(distribution(generator)*0.1);}

        if((i==3) && (j!=0) && (j !=nfibers_-1)){
	  //Direct
	  fibers[3][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[3][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[2][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[2][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[4][j+1]->addManyPhoton(distribution(generator)*0.1);}
     
	if((i==4) && (j!=0) && (j !=nfibers_-1)){
	  //Direct
	  fibers[4][j-1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  fibers[4][j+1]->addManyPhoton(distribution_direct(generator_direct)*0.1);
	  //Diagonal
	  fibers[3][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[3][j+1]->addManyPhoton(distribution(generator)*0.1);
	  fibers[5][j]->addManyPhoton(distribution(generator)*0.1);
	  fibers[5][j+1]->addManyPhoton(distribution(generator)*0.1);}
 
        }
      }
    }
  } 
  
sig_channels->InitializePhotons();
  //Fill the pixels 

  for(unsigned int i=0;i<layers_;i++) {
    for(unsigned int j=0;j<nfibers_;j++) {
      //std::cout<<"################### i , j "<<i<<" "<<j<<" number of photons: "<<fibers[i][j]->getNumbPhotons()<<std::endl;     
      sig_channels->fillPixels(fibers[i][j]->getX(),fibers[i][j]->getY(),fibers[i][j]->getNumbPhotons(),fibers[i][j]->getDiameter(), method, angle_cut);
    }
  }
  //sig_channels->Debug();
  //Fill the channels
  sig_channels->InitializePixels();
  sig_channels->fillChannels(); 

  //Drawing
  TParticle->SetX1(x0-sig_channels->getHight()/2.0*tan(atan(tan(theta)*cos(phi)))); 
  TParticle->SetX2(x0+sig_channels->getHight()/2.0*tan(atan(tan(theta)*cos(phi)))); 
  TParticle->SetY1(-sig_channels->getHight()/2.0);
  TParticle->SetY2(sig_channels->getHight()/2.0);
}


unsigned int SignalSetup::CheckChannel(unsigned int const& index) const {
    return sig_channels->PixInChannel(index);
}

unsigned int SignalSetup::CheckPhotonChannel(unsigned int const& index) const {
    return sig_channels->PhotInChannel(index);
}

void SignalSetup::Reset() {
    sig_channels->Reset();
    for(unsigned int i=0;i<layers_;i++) {
        for(unsigned int j=0;j<nfibers_;j++) {
            fibers[i][j]->UnCheck();
            fibers[i][j]->setNumbPhotons(0);
        }
    }
}

SignalChannels* SignalSetup::getSigChannels() const {
    return sig_channels;
}


vector<TEllipse*> SignalSetup::getTFibers() const {
    return TFibers;
}


vector<TBox*> SignalSetup::getTChannels() const {
    return TChannels;
}

TLine* SignalSetup::getTParticle() const {
    return TParticle;
}

void SignalSetup::setRNG(TRandom3* rand) {
    delete(random);
    random=rand;
}


