/*
 * SuperDetector.cc
 *
 *  Created on: Jan 22, 2013
 *      Author: bruggiss
 */

#include "SuperDetector.h"
#include <ctime>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include "TError.h"
#include "TString.h"
#include <TFile.h>
#include <TTree.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */


SuperDetector::SuperDetector(double const& fp, double const& ct,
        double const& tw, double const& iw,double const& rt,double const& q,double const& gainsigma,double const& rs,double const& rq,
        double const& cg,double const& ceq,double const& cd,double const& cq,
        double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT,double const& elect, Shaper const& shape,
        unsigned int const& numVal, double const& timeMax,double const& diam, double const& diamSigma, double const& gap, double const& posSigma,
        double const& channelWidth, double const& channelHight, unsigned int const& numbOfDistr,double const& QE, double const& epoxy,unsigned int const& layers,  unsigned int const& nfibers,double const& fiber_length,
        double const& brewster, double const& reflectivity, double const& attlen,
			     double const& fxt, double const& dfxt) :
        Detector(1600,fp,ct,tw,iw,rt,q,gainsigma,rs,rq,cg,ceq,cd,cq,prob,weightSl,tauSl,tauFa,maxT,elect,shape,numVal,timeMax),
        signal_setup(new SignalSetup(diam,diamSigma,gap,posSigma,channelWidth,channelHight,epoxy,new TRandom3((time(NULL)*1000000.0+time(NULL)*1000000.0+time(NULL)*1000000.0+time(NULL)*1000000.0)/4000000.0),numbOfDistr,QE,layers,nfibers,brewster, reflectivity, attlen)),random(new TRandom3(time(NULL))){ 
    signal_setup->setRNG(random);
    double Dia=diam;
    double DSigma=diamSigma;
    double GAP=gap;
    double PosSigma=posSigma;
    double chWidth=channelWidth;
    double chHight=channelHight;
    unsigned int numD=numbOfDistr;
    double epox=epoxy;
    unsigned int const entries=signal_setup->getSigChannels()->getRandomPhi().size();
    double randPhi[entries];
    unsigned int nVal=numVal;
    double tMax=timeMax;
    for(unsigned int i=0;i<entries;i++){
        randPhi[i]=signal_setup->getSigChannels()->getRandomPhi()[i]*180.0/M_PI;
    }
    settings->Branch("NumberOfValuesInShaper",&nVal,"nVal/I");
    settings->Branch("MaxTimeStockShaper",&tMax,"tMax/D");
    settings->Branch("DiameterOfFiber",&Dia,"Dia/D");
    settings->Branch("SigmaOnDiameterOfFiber",&DSigma,"DSigma/D");
    settings->Branch("GapBetweenFibers",&GAP,"GAP/D");
    settings->Branch("SigmaOnPositionOfFiberCenter",&PosSigma,"PosSigma/D");
    settings->Branch("ChannelWidth",&chWidth,"chWidth/D");
    settings->Branch("ChannelHight",&chHight,"chHight/D");
    settings->Branch("NumberOfEntriesLookupDistribution",&numD,"numD/I");
    settings->Branch("ThicknessOfEpoxy",&epox,"epox/D");
    settings->Branch("RandomPhi",&randPhi,TString::Format("randPhi[%i]",entries));
    settings->Fill();
}

void SuperDetector::GenerateSignal(double const& thetaMax,unsigned int const& meanPhotons, double const& sigma,
				   unsigned int const& spareChannels, double const& probFiberxTalk, unsigned int const& Rand, unsigned const& method, unsigned int const& layers, unsigned int const& nfibers, double const& fiber_length, double const& fxt, double const& dfxt,double const& angle_cut, int i) {
//std::cout<<"entering GenerateSignal "<<i<<std::endl;
    //--------Configuration 1 scintillating fiber layer---------
    Layer L1; // first fiber layer
    L1.angle=0.0;
    L1.width=40.0;
    L1.length=40.0;
    L1.x=20.0;
    L1.y=20.0;
    L1.z=0.0;
    Layers.push_back(L1);

    //*********New method for angle distribution***************
    double theta(0.0); // Angle with respect to the z axis in a vertical plane
    double phi(0.0); // Angle in the xy plane
    double anglex(0.0); // Angle of the projection of the track in the xz plane
    double angley(0.0); // Angle of the projection of the track in the yz plane
        
    //*********************************************************

    // Different methods to generate the angle theta
    // thetaMax is the parameter given as input
    //     1 uniform distribution in [-thetaMax,+thetaMax]
    //     2 exponential distribution (i.e. distribution is exp(-theta/thetaMax))
    //     3 gaussian distribution (mean = 0 and sigma = thetaMax)
    //    4 uniform distribution in (+/-)]thetaMax-margin,thetaMax+margin] with margin = 5° --> Change below
    //    5 distribution proportional to cos(theta)^2 in (+/-)]thetaMax-margin,thetaMax+margin] with margin = 2°, phi=0
    //    6 distribution proportional to cos(theta)^2 in [0,pi/2] with acceptance of telescope
    double margin = 2.0*M_PI/180.0;
    //double margin = 90.0*M_PI/180.0; // pour avoir +/- pi/2
    if(Rand==1) {
           theta=random->Rndm()*(2.0*thetaMax)*M_PI/180.0-thetaMax*M_PI/180.0;          // Uniformly distributed angle between pi/2 and -pi/2
          //theta=random->Rndm()*(thetaMax)*M_PI/180.0;                                 // Assuming thetaMax is given in degrees !!! 
       
    } else if(Rand==2) {
        theta=random->Exp(thetaMax)*M_PI/180.0;
        if(random->Rndm()<0.0){ // ?? random->Rndm() always positive (in [0,1])
            theta=random->Exp(thetaMax*5)*M_PI/180.0;
        }
        if(random->Rndm()>0.5){ // prob 0.5 to have a negative angle (gives symmetric distr)
            theta=-theta;
        }
    } else if(Rand==3) {
          theta=random->Gaus(0.0,thetaMax)*M_PI/180.0;
        //rita momentanea modifica
        //  theta=random->Gaus(thetaMax,0.5)*M_PI/180.0;
        // fine modifica rita
    } else if(Rand==4) {
        theta = thetaMax*M_PI/180.0 + margin - random->Rndm()*2.0*margin;
        if(random->Rndm()>0.5){ // prob 0.5 to have a negative angle (gives symmetric distr)
            theta=-theta;
        }
    } else if(Rand==5){
        unsigned int Ntheta = 100; // partition for the interval [thetaMax-margin,thetaMax+margin]
        unsigned int rIndex = 0;
        double randomtheta=random->Rndm();
    
        double coeff = 0.0;     
    
        for(unsigned int k = 0; k<Ntheta; ++k){
            double thetak = thetaMax*M_PI/180.0+margin*((1.0+2.0*k)/Ntheta-1.0);
            coeff+=cos(thetak)*cos(thetak);
        }
        coeff = 1.0/coeff;
    
        double theta0 = thetaMax*M_PI/180.0+margin*(1.0/Ntheta-1.0);
        double sumP = coeff*cos(theta0)*cos(theta0);
    
        while(randomtheta > sumP){
            ++rIndex;
            double thetarIndex = thetaMax*M_PI/180.0+margin*((1.0+2.0*rIndex)/Ntheta-1.0);
            sumP+=coeff*cos(thetarIndex)*cos(thetarIndex);
        }
        theta = thetaMax*M_PI/180.0+margin*((1.0+2.0*rIndex)/Ntheta-1.0)+margin/Ntheta*(2*(random->Rndm())-1.0);
        if(random->Rndm()>0.5){ // prob 0.5 to have a negative angle (gives symmetric distr)
            theta=-theta;
        }
    } else if(Rand==6){
        do{
            phi = random->Rndm()*2.0*M_PI;
            unsigned int Ntheta = 1000; // partition for the interval [0,pi/2]
            unsigned int rIndex = 0;
            double randomtheta=random->Rndm();
            double coeff = 0.0;

            for(unsigned int k = 0; k<Ntheta; ++k){
                double thetak = 0.5*M_PI/Ntheta*(0.5+k);
                coeff+=cos(thetak)*cos(thetak)*sin(thetak);// sin(theta) added for solid angle
            }
            coeff = 1.0/coeff;

            double sumP=coeff*cos(M_PI/(4*Ntheta))*cos(M_PI/(4*Ntheta))*sin(M_PI/(4*Ntheta));

            while(randomtheta > sumP){
                ++rIndex;
                double thetarIndex = 0.5*M_PI/Ntheta*(0.5+rIndex);
                sumP+=coeff*cos(thetarIndex)*cos(thetarIndex)*sin(thetarIndex);// sin(theta) added for solid angle
            }

            theta = 0.5*M_PI/Ntheta*(0.5+rIndex) + (random->Rndm()-0.5)*M_PI/(2*Ntheta);
            anglex=atan(tan(theta)*cos(phi));
            angley=atan(tan(theta)*sin(phi));
        }while(!(TrackInAllLayers(anglex,angley)));
    }
//version with and without TrackInAllLayers
   /* else if(Rand==7){do{
	theta=m_thetaangle.at(i);
	phi=m_phiangle->at(i);}while(!(TrackInAllLayers(anglex,angley,i)));
        }*/
	else if(Rand==7){
	         phi=m_thetaangle.at(i);
        	 theta=m_phiangle.at(i);
         }
    
    //*********New method for angle distribution***************
    anglex=atan(tan(theta)*cos(phi));
    angley=atan(tan(theta)*sin(phi));
    //*********************************************************
    //rita modification
     
    double width = signal_setup->getSigChannels()->getWidth();
//     std::cout<<"getSigChannels: "<<std::endl;
//    double ParticleChannel = random->Rndm()*(1600-2*spareChannels)+ spareChannels;
//    Double_t x0=(ParticleChannel+0.0)*width +spareChannels*width;    
      Double_t x0=(m_x.at(i)+20)/100;
      //std::cout<<x0<<std::endl;
      double ParticleChannel=x0/width-spareChannels;
      //std::cout<<" Particle channel "<<ParticleChannel<<std::endl;
//    std::cout<<"************************"<<x0<<" "<<width<<" "<<ParticleChannel<<std::endl;
    thetaIntersept.push_back(theta);
    phiIntersept.push_back(phi);
    thetaxIntersept.push_back(anglex);
    thetayIntersept.push_back(angley);
    //std::cout<<"x0Intersept :"<<x0*1000.0<<std::endl;
    x0Intersept.push_back(x0*1000.0);
    chIntersept.push_back(ParticleChannel+spareChannels);
    //std::cout<<"x0Intersept : "<<x0*1000.0<<" chIntersept: "<<ParticleChannel+spareChannels<<std::endl;
    //*********New method for angle distribution***************
    //create a particle at (x0, theta)
    //calculate chords in every fibre that is crossed by the particle
    //generate number of photons (Poisson with mean proportional to the chord)
    //ditribute those photons uniformly over the whole fibre cross section
   //  signal_setup->SimulateParticle(theta,0,x0,meanPhotons,sigma,probFiberxTalk, method);
    //"light" the pixels that lie under photons
    theta=anglex;
//    std::cout<<" before SimulateParticle"<<std::endl;
    signal_setup->SimulateParticle(theta,angley,x0,meanPhotons,sigma,probFiberxTalk, method, layers, nfibers,fiber_length,fxt,dfxt, angle_cut);
//    std::cout<<" after SimulateParticle"<<std::endl;
    //*********************************************************   
    //run the detector simulation (crosstalk, after pulses, shper, etc) 
    double Max=RAND_MAX;
    for(unsigned int i=0;i<NumOfChannels;++i){
//        std::cout<<"before loop"<<std::endl;
        for(unsigned int j=1;j<=signal_setup->CheckChannel(i);j++){    
            //fired pixels in channel
         //if(j==1 && signal_setup->CheckChannel(i)>1)std::cout<<" for loop "<<i<<" "<<signal_setup->CheckChannel(i)<<std::endl;  
            double t=(timeWindow-integrationWindow+Channels[i]->getShaper()->getTimeAtMax());
            double u=rand()/Max;
            unsigned int num(0);
            do {
            u=rand()/Max;
            num++;                  
            } while(u<probCrossTalk);
           //std::cout<<" while crosstalk "<<j<<std::endl;
            NoiseHit* hit = new NoiseHit(t,num);  
            hit->generateAfterpulses(Channels[i]->getRiseTime(),Channels[i]->getTauR(),probAfter,weightSlow,tauSlow,tauFast,maxTime); 
         
            //std::cout<<" after generateAfterpulses "<<j<<std::endl;
            CheckSignalAfterPulses(Channels[i],hit);
            //std::cout<<" after CheckSignalAfterPulses "<<j<<std::endl;
            Channels[i]->addSignalHit(hit);
          // std::cout<<"Channel: "<<i<<" with hit: "<<
          
        }
//std::cout<<"after loop"<<std::endl;
    }
}   


bool SuperDetector::TrackInAllLayers(double const& thetax, double const& thetay, int i){
    // This method picks a random position on the first layer of Layers and checks if the track goes through all 
    //the other layers
    
    double x0(0.0);
    double y0(0.0);
    //x0=Layers[0].x+Layers[0].width*(random->Rndm()-0.5);
    //y0=Layers[0].y+Layers[0].length*(random->Rndm()-0.5);
    x0=m_x.at(i);
    y0=m_y.at(i);
    bool TrackInLayer(0);
    unsigned int layerIndex(1);
    unsigned int numberOfLayers(Layers.size());
    int counter =0;
    do{
        double alpha(Layers[layerIndex].angle);
        double xtrack(x0+tan(thetax)*(Layers[layerIndex].z-Layers[0].z));
        double ytrack(y0+tan(thetay)*(Layers[layerIndex].z-Layers[0].z));
        double yA(Layers[layerIndex].y-0.5*Layers[layerIndex].length);
        double yD(yA+Layers[layerIndex].length);
        double xA(Layers[layerIndex].x-0.5*Layers[layerIndex].width+tan(alpha)*(ytrack-yA));
        double xB(xA+Layers[layerIndex].width);
        //std::cout<<alpha<<std::endl;
        //std::cout<<ytrack<<" >= "<<yA<<" <=  "<<yD<<" "<<xtrack<<" >= "<<xA-tan(alpha)*(ytrack-yA)<<" <= "<<xB-tan(alpha)*(ytrack-yA)<<std::endl;
        if((ytrack>=yA)&&(ytrack<=yD)&&(xtrack>=xA-tan(alpha)*(ytrack-yA))&&(xtrack<=xB-tan(alpha)*(ytrack-yA))){
            TrackInLayer=true;
        }else{
            TrackInLayer=false;
        }
        ++layerIndex;
    }while(TrackInLayer && (layerIndex<numberOfLayers) );
    return TrackInLayer;
}

void SuperDetector::CheckSignalAfterPulses(Channel* chan, NoiseHit* const& hit) {
    double Max=RAND_MAX;
       
    for(unsigned int k=0;k<hit->geteNumberOfAfterpulses();k++){ //getnumebr of afterpulses ritorna un tot numero di vettori di afterpulses eguale al numero di pixel colpiti
        
       for(unsigned int i=0;i<hit->getAfterPulses()[k]->getTime().size();i++){   //per ogni pixel abbiamo poi un vettore perche' gli afterpulses per pixel possono essere 
            double tAfter=hit->getAfterPulses()[k]->getTime()[i];  //per l'iesimo afterpulses del k pixel ottenimo il tempo
            double intensAfter=hit->getAfterPulses()[k]->getIntensity()[i]; ////per l'iesimo afterpulses del k pixel ottenimo l'intensita'
            unsigned int numAfter(0);
            double r=rand()/Max;
        //    cerr << "i = " << i << endl;
        //    cerr << "k = " << k << endl;
            while(r<intensAfter*probCrossTalk){
                r=rand()/Max;
                numAfter++;  
            }
            if(numAfter>0){
                NoiseHit* hitAfter = new NoiseHit(tAfter,numAfter);
                hitAfter->generateAfterpulses(chan->getRiseTime(),chan->getTauR(),probAfter,weightSlow,tauSlow,tauFast,maxTime);
                CheckSignalAfterPulses(chan,hitAfter);
                chan->addSignalHit(hitAfter);
            }
        }
    }
}


void SuperDetector::generateDataWriteROOT(double const& thetaMax,unsigned int const& meanPhotons, double const& sigma,unsigned int const& spareChannels, double const& probFiberXTalk, unsigned int const& Rand, unsigned int const& method,unsigned const& layers, unsigned const& nfibers,double const& fiber_length, double const& fxt,double const& dfxt, double const& angle_cut, std::vector<double> x,std::vector<double> y,std::vector<double> z,std::vector<double> thetaangle,std::vector<double> phiangle,std::vector<double> Ptot,std::vector<double> PdgID,std::vector<double>& ch,std::vector<std::vector<double>>& L1_S1_N,std::vector<std::vector<double>>& L1_S1_S,std::vector<std::vector<double>>& L1_S1_P,std::vector<std::vector<double>>& L1_S1_D) {
    cerr << "Data simulation in course" << endl;

    m_x=x;
    m_y=y;
    m_z=z;
    m_thetaangle=thetaangle;
    m_phiangle=phiangle;
    m_Ptot=Ptot;
    m_PdgID=PdgID;
    
    unsigned int events=m_x.size();
    thetaIntersept.clear();
    phiIntersept.clear();
    thetaxIntersept.clear();
    thetayIntersept.clear();
    chIntersept.clear();
    x0Intersept.clear();
    Layers.clear();

    int bar=0;
    //->InitPixelsandPhotons();
    double dcr(0.0);

    for(unsigned int i=0;i<m_x.size();i++){

        GenerateNoise();
	
        GenerateSignal(thetaMax,meanPhotons,sigma,spareChannels,probFiberXTalk,Rand, method,layers,nfibers,fiber_length,fxt,dfxt, angle_cut,i);

        L1_S1.clear();
        for(unsigned int j=0;j<NumOfChannels;j++){
            L1_S1.push_back( Channels[j]->ADCs(timeWindow,integrationWindow,electSigma) );
            dcr+=Channels[j]->DCR(timeWindow,integrationWindow);
         }
        L1_S1_N.push_back(L1_S1);
        L1_S1.clear();
        for(unsigned int j=0;j<NumOfChannels;j++){
            L1_S1.push_back(Channels[j]->ShaperSignalADCs(timeWindow,integrationWindow,electSigma));
         }
        L1_S1_S.push_back(L1_S1);
        L1_S1.clear();
     
        for(unsigned int j=0;j<NumOfChannels;j++){
                L1_S1.push_back(Channels[j]->ShaperSignalADCs(timeWindow,integrationWindow,electSigma) + Channels[j]->ShaperNoiseADCs(timeWindow,integrationWindow,electSigma));
        }
        L1_S1_D.push_back(L1_S1);
        L1_S1.clear();
        signal_setup->Reset();
        Reset();
        GenerateNoise();

         for(unsigned int j=0;j<NumOfChannels;j++){
            L1_S1.push_back(Channels[j]->ShaperNoiseADCs(timeWindow,integrationWindow,electSigma));
        }
        L1_S1_P.push_back(L1_S1);
        L1_S1.clear();
        Reset();
     
        if(events>100 && i%(events/100)==0 && bar<100) {
            cerr << "#";
            bar++;
        }

    }
    dcr=dcr/(NumOfChannels*events);


    for(unsigned int k=0;k<events;k++) {

        ch.push_back(chIntersept[k]);
    }

}
