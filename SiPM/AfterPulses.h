/*
 * AfterPulses.h
 *
 *  Created on: Dec 7, 2012
 *      Author: bruggiss
 */

#ifndef AFTERPULSES_H_
#define AFTERPULSES_H_

#include <vector>

using namespace std;

/*
 * This class describes the after pulses. at each pulse from a pixel there might be attributed an after pulse (or not depending on the probability). An after pulse is
 * characterized by its time of occurrence and its intensity. As every pixel can have more than one after pulse, this information are stored in a vector.
*/
class AfterPulses{
public:
    AfterPulses(); //there is only a default constructor because the class doesn't have any attributes that are defined at the construction.
    void generateAfterPulses(double const& t0, double const& recoveryT,
            double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT); //generates afterpulses with a probability prob and following
                                                                                                                        //a distribution charaterized by weightSl, tauSl, tauFa, and maxT

    vector<double> getTime() const; //returns the vector time
    vector<double>    getIntensity() const; //returns the vector intensity

private:
    vector<double> time; //the time at which the afterpulse happens with respect to time 0 (see class detector, zero of timeWindow)
    vector<double>    intensity;    //Relative intensity of the afterpulse normalized to the intensity of one pixels intensity

};


#endif /* AFTERPULSES_H_ */
