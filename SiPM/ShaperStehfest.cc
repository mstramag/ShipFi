/*
 * ShaperStehfest.cc
 *
 *  Created on: Jan 7, 2013
 *      Author: bruggiss
 */

#include "ShaperStehfest.h"
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>


ShaperStehfest::ShaperStehfest(double const& tC, double const& tS, unsigned int const& n) : Shaper(tC,tS,n){
    if(N%2!=0){
        N+=1;
    }
    for(unsigned int i=1; i<N;i++){
        unsigned int kmax=i;
        if(kmax>N/2.0){
            kmax=N/2.0;
        }
        double v=0.0;
        //cerr << "just before factorial" << endl;
        for(unsigned int k=floor((i+1.0)/2);k<=kmax;k++){
            try{
                v+=pow(k,N/2.0)*factorial(2*k)/(factorial(N/2-k)*factorial(k)*factorial(k-1)*factorial(i-k)*factorial(2*k-i));
            } catch(string& a){
                cout << a << endl;
            }
            //cerr << "just after factorial" << endl;
        }
        v_i.push_back(pow(-1,(N/2.0+i))*v);
    }
}

ShaperStehfest* ShaperStehfest::clone() const {
    return new ShaperStehfest(*this);
}

double ShaperStehfest::OutFunction(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& t) const {
    double ln2=log(2.0);
    double intermediate=0.0;
    for(unsigned int i=1;i<=N;i++){
        intermediate+=v_i[i]*funcInLaplace(tauQ,tauIn,tauR,riseTime,i*ln2/t);
    }
    return ln2/t*intermediate;
}

double ShaperStehfest::OutputForStandardInput(double const& t, bool const& normalized) const {
    double ln2=log(2.0);
    double intermediate=0.0;
    for(unsigned int i=1;i<=N;i++){
        intermediate+=v_i[i]*standardFuncInLaplace(i*ln2/t);
    }
    return ln2/t*intermediate;
}


double ShaperStehfest::funcInLaplace(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& s) const {
    double rise=((tauQ/(tauIn*tauR))/riseTime)*exp(-riseTime*s)*(-riseTime*s+exp(riseTime*s)-1.0)/(s*s);
    double a=1.0/(tauR-tauIn);
    double b=(tauQ-tauIn)/tauIn;
    double c=tauIn;
    double d=(tauR-tauQ)/tauR;
    double e=tauR;
    double fall=a*(b*c*exp(-riseTime*s)/(c*s+1.0)+d*e*exp(-riseTime*s)/(e*s+1));
    double csa=s*timeCSA/(s+1.0/timeCSA);
    double shape=s/((s+1.0/timeShape)*(s+1.0/timeShape));
    return (rise+fall)*csa*shape;
}

double ShaperStehfest::standardFuncInLaplace(double const& s) const {
    double csa=s*timeCSA/(s+1.0/timeCSA);
    double shape=s/((s+1.0/timeShape)*(s+1.0/timeShape));
    return csa*shape;
}


int ShaperStehfest::factorial(int const& r) const {
    if(r>50){
        stringstream str;
        str << setprecision(10) << r;
        str << " is a to big of a number for the calculation of the factorial";
        string str_to_string=str.str();
        throw str_to_string;
    }
    if(r<0){
        return 1;
    }
    if(r==0){
    //    cerr << "now it is 0" << endl;
        return 1;
    }
    //cerr << "r = " << r << endl;
    return r*factorial(r-1);
}
