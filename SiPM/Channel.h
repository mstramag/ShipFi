/*
 * Channel.h
 *
 *  Created on: Nov 30, 2012
 *      Author: bruggiss
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_

#include "NoiseHit.h"
#include "AfterPulses.h"
#include "ShaperStehfest.h"
#include "ShaperFourier.h"
#include <vector>

using namespace std;

/*
 * The class Channel represents a channel of a SiPM. As the noise is only known as mean values and spectra, we don't simulate every pixel but we simulate everything in term of
 * "noise hits" which are created following the probabilities known for the SiPM
 */


class Channel {

public:
    Channel(double const& rt,double const& q,double const& rs,double const& rq,
        double const& cg,double const& ceq,double const& cd,double const& cq, Shaper* const& shape,unsigned int const& numVal,double const& timeMax);
    ~Channel();

    NoiseHit* getNoiseHits(unsigned int const& index) const; //returns the pointer to the noise hit index, for lecture only
    NoiseHit* getNoiseHits(unsigned int const& index);    //return the pointer on the noise hit index, read and write.

    unsigned int getHitsNumber() const; //returns the number of noise hits in the collection.

    void ResetHits(); //resets the noise hits collection to zero hits.

    int ADCs(double const& timeWindow, double const& integrationWindow,double const& electNoiseSigma) const; //returns the adc counts of the noise created in the timeWindow (be sure the time window is the same as the one used for the noise creation!!)
    int ShaperNoiseADCs(double const& timeWindow, double const& integrationWindow, double const& electNoiseSigma) const;    //the noise that has passed through the shaper
    int ShaperSignalADCs(double const& timeWindow, double const& integrationWindow, double const& electNoiseSigma) const;    //the signal that has passed through the shaper
    double DCR(double const& timeWindow, double const& integrationWindow)const; //Dark current rate
    double OutCurrent(double const& t,double const& intens) const; //gives the current for a pulse at a time t after the time zero for the pulse, i.e. t is the time after the pixel has been fired.
    double OutCurrentForPulseShape(double* x, double* p); // in order to define a function that can be drawn by root
    double ShaperOut(double const& t) const; //the output current from the shaper (not normalized yet!!)
    double ShaperStandardOut(double const& t) const; //the output current for the standard input from the shaper (not normalized yet!!)
    double AnalyticalStandardShaper(double const& t) const; //the analytical output function for a delta input.
    double DiscreteOutput(double const& t, bool const& normalized) const; //use the discrete output function for time saving.
    void addNoiseHit(NoiseHit* noise); // add a noise hit to the collection.
    void addSignalHit(NoiseHit* sign); // add a noise hit to the collection.

    Channel* Copy() const; //Copies the channel and creates a new one with exact the same properties, it returns the pointer to the new Channel

    double getTauR() const; //returns the slow time constant of the pulse of one fired pixel

    double getRiseTime() const; //return the value of the rise time.

    Shaper* getShaper() const; //return the Shaper;

private:
    Shaper* shaper;
    vector<NoiseHit*> hits;        //the collection of noise hits
    vector<NoiseHit*> signal;            //the signal hit and it's afterpulses.

    /*
     * The following parameter are the ones who characterize the pulse-shape. they are measured if possible and estimated for the others.
     */

    double RiseTime;    //the rise time of the signal, in seconds
    double Q;            //the area under the curve of the pulse shape (if integrated over the whole pulse), usually it can be used to get the right gain. for example: if Q=50 the value of the one photon-peak will be at about 50 adcs (not exaclty 50 due to the fact that not the whole signal is integrated and that there is some electrical noise)
    double Rs;            //the Rs resistance, given in Ohm (normally about 20ohm)
    double Rq;            //the Rq resistance, given in Ohm (normally around 100kOhm)
    double Cg;            //Capacitance Cg, in Farad (in the order of pico-farad)
    double Ceq;            //Capacitance Ceq, in Farad (in the order of pico-farad)
    double Cd;            //Capacitance Cd, in Farad (in the order of pico-farad)
    double Cq;            //Capacitance Cq, in Farad (in the order of pico-farad)


    double getTauIn() const;    //is the fast time constant of the pulse of one fired pixel
    double getTauQ() const;    //another time constant, only used for renormalization of the signal
    double ElectronicNoise(double const& mean, double const& sigma) const;    //creates a gaussian around mean with a width of sigma and returns a double following the probability distribution represented by this gaussian
    double gaussian(double const& mean, double const& sigma, double const& x) const; //returns the value of a gaussian mith mean: mean and width: sigma, at the position x

    double integrateCurrent(double const& t0, double const& intens,double const& timeWindow, double const& integrationWindow) const; //Integrates the pulse of a pixel that has been fired at a time t0 (with respect to the timeWindw). The integration is only done for the part of the signal that is contained in the integrationWindow. The double intens is needed to take into account the cross talk (for example the intens will be 2 if two pixels are fired due to cross talk) and afterpulsing (an afterpulses intensity varies between 0 and 1=
    int round(double const& x) const; //rounds of the double x and returns an int. this is used to convert the integrated current into adc counts.


};


#endif /* CHANNEL_H_ */
