/*
 * Shaper.cc
 *
 *  Created on: Jan 8, 2013
 *      Author: bruggiss
 */

#include "Shaper.h"
#include <cmath>
#include <iostream>
#include <cstdio>

Shaper::Shaper(double const& tC, double const& tS, unsigned int const& n) : timeCSA(tC),timeShape(tS),N(n),max(0.0),timeAtMax(0.0),maxStandard(0.0),maximalTime(0.0),dt(0.0){}

Shaper::~Shaper(){}

double Shaper::getTimeCSA() const {
    return timeCSA;
}

double Shaper::getTimeShape() const {
    return timeShape;
}

double Shaper::getN() const {
    return N;
}

double Shaper::geta() const {
    return 0.0;
}

void Shaper::initialize(double const& rt,double const& rs,double const& rq,
        double const& cg,double const& ceq,double const& cd,double const& cq,unsigned int const& steps, double const& maxTime) {
    //maxTime: The maximal time up to which the values are stored in each shaper

    for(unsigned int j=0;j<100;j++){
        cerr << "#";
    }
    cerr << endl;
    cerr << "Initializing Shaper  " << endl;
    double DT=maxTime/steps;
    maximalTime=maxTime;
    Values.push_back(0.0);
    /* * * * *
     * rq: the quenching resistor
     * cq: a quenching capacitance -- a larger quenching capacitance will mostly increase the single photoelectron pulse height.
     * rs: the current signal of the SiPM is converted into a voltage by means of the resistor rs 
     * cg: the grid capacitance -- Increasing the total area of a SiPM will also increase the grid capacitance which will then reduce the pulse height and stretch its width in time. 
     * ceq: the series connection between (N-1)cd and (N-1)cq 
     * cd: the (single cell) diode capacitance -- a lower cell capacitance leads to a faster rise time
     * * * * *
     * The current Is(t) (though rs) in the response to an input current pulse of area Q:
     * Is(t) ~ Q/(tauQ-tauIn)*[(tauQ-tauIn)*tauIn*exp(-t/tauIn)+(tauR-tauQ)/tauR*exp(-t/tauR)]
     * tauIn depends on rs
     * tauR is the recovery time constant of the detector 
     * */
    double tauQ=rq*cq;
    double tauIn=rs*(cg+ceq);
    double tauR=rq*(cd+cq);
    cerr << "#";
    int bar=0;
    maxStandard=0;
    for(unsigned int i=1;i<steps;i++) {
        if(i%(steps/100)==0 && bar < 100){
            cerr << "#";
            bar++;
        }
        Values.push_back(OutFunction(tauQ,tauIn,tauR,rt,i*DT));
        //cerr << Values[i] << endl;
        if((Values[i-1]>Values[i] && max==0) || (max < Values[i-1] && max!=0)){
            max=Values[i-1];
            timeAtMax=(i-1.0)*DT;
        }
        if(maxStandard < OutputForStandardInput(i*DT,false)){
            maxStandard=OutputForStandardInput(i*DT,false);
            //cerr << maxStandard << endl;
        }
    }
    cerr << endl;
    dt=maximalTime/Values.size();
}

double Shaper::DiscreteOutFunction(double const& t, bool const& normalized) const {
    if(t<=0 || t>maximalTime) {
        return 0.0;
    }
    int index=round(t/dt);
    //cerr << Values[index] << endl;
    if(normalized) {
        return Values[index]/max;
    }
    return Values[index];
}


int Shaper::round(double const& x) const {
    double t=x-floor(x);
    if (t>=0.5) {
        return ceil(x);
    }
    else {
        return floor(x);
    }
}

double Shaper::getMaxStandard() const {
    return maxStandard;
}

double Shaper::getMax() const {
    return max;
}

double Shaper::getTimeAtMax() const {
    return timeAtMax;
}
