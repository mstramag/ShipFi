/*
 * SignalFiber.cc
*
*  Created on: Jan 21, 2013
 *      Author: bruggiss
 */

#include "SignalFiber.h"
#include <cmath>
#include <ctime>
#include <iostream>
#include "TF1.h"
#include "TMath.h"

SignalFiber::SignalFiber():photons(0),x(0.0),y(0.0),diameter(0.0),random(new TRandom3(time(NULL))),checked(false) {}; 



SignalFiber::SignalFiber(double const& X, double const& Y, double const& posSigma, double const& limit, double const& diam, double const& diamSigma, TRandom3* rand):
photons(0),x(0.0),y(0.0),diameter(0.0),random(new TRandom3(time(NULL)*(X+Y)/(sqrt(X*X+Y*Y)/2.0))),checked(false) { 
    
  diameter=random->Gaus(diam,diamSigma);

  double r=0.0;

  /* In this section, we initialie a fiber. The real position is given by the exact position in the fiber lattice (X, Y) to 
    which a random deviation distributed after posSigma in a random direction ( r* cos(thete), r*sin(thetais)) is added. */
    
  do {
    r=random->Gaus(0,posSigma);         // Choose a random deviation with a Gaussian distribution centered in 0 and with sigma posSigma
  } while (r>limit);                      // Making sure the deviation si smaller than half the gap between fibers
  double theta=random->Rndm()*2.0*M_PI;   // Choosing a random angle between 0 and 2pi in which the deviation happens
    
  x=X+r*cos(theta);
  y=Y+r*sin(theta);              // Setting the real position (with error)
}

// Get the impact parameter followin equation given in s. Bruggisser notice
double SignalFiber::getImpactP(double const& theta, double const x0) const {
  double y0=0.0;
  double xj = x0 + sin(theta)*sin(theta)*(x-x0) + sin(theta)*cos(theta)*(y-y0);
  double yj = y0 + sin(theta)*cos(theta)*(x-x0) + cos(theta)*cos(theta)*(y-y0);
  //std::cout<<"x0: "<<x0<<std::endl;
  //std::cout<<"xj: "<<xj<<std::endl;
  //std::cout<<"yj: "<<yj<<std::endl;
  //std::cout<<"x: "<<x<<std::endl;
  //std::cout<<"y: "<<y<<std::endl; 
  return sqrt((x-xj)*(x-xj)+(y-yj)*(y-yj));
}

// Get the Chord of the particle in the fibre.
// Diameter was reduced of 30um due to non generation of photon in the double cladding

double SignalFiber::getChord(double const& theta, double const& phi, double const& x0) const {
 
  double P=getImpactP(theta,x0);
  //std::cout<<"getImpactP: "<<P<<std::endl;
  if(P<=(diameter-30.0e-6)/2.0) {
//    std::cout<<"getImpactP: "<<P<<std::endl;
    return 2.0*sqrt((diameter-30.0e-6)*(diameter-30.0e-6)/4.0-P*P)/cos(phi);
  }
  return 0.0;
}

// Function that generate photons in the fibre.
// The generation depends of the chord and follows a tunable landau distribution.
// mu and sigma from distribution is given in the config file 

void SignalFiber::createPhotons(double const& theta, double const& phi, double const& x0, unsigned int const& mean, double const& sigma,double excitation_point,double const& fiber_length, double ph_trapped_emi, double mirror_reflectivity, double att_lenght,double fxt) {
  //double mirror_position=0.;
  double mirror_position = 40.0;
  double photons_to_neighboors =0.;
  double length_to_SiPM=fiber_length-excitation_point;
  //double length_to_mirror=excitation_point-mirror_position;
  double length_to_mirror=mirror_position-excitation_point;
  //Get the chord of the particle in the fibre
  double C=getChord(theta,phi,x0);
  //if(C>0)std::cout<<"chord: "<<C<<std::endl;   
  if(C>0) {
  
    //Generate the mean number of photon following the Landau distribution    
    double mean_number=random->Landau(mean*C,sigma);
    //std::cout<<mean_number<<std::endl;
    while (mean_number<0){
      mean_number=random->Landau(mean*C,sigma);
    }
    //Trapped photon in the SiPM
    double photons_to_SiPM=ph_trapped_emi*(mean_number);

    //Number of photon that could travel to neighbooring channels (UV photons)
    //#UVphotons = #Total - #Trapped
    photons_to_neighboors=(1-(2*ph_trapped_emi))*(mean_number);

    //cout << "photon to neighboor" << photons_to_neighboors <<" "<<ph_trapped_emi<<endl;

    //Attenuation of the UV photons (done before the xt-probability-> not sure if it is the proper way)
    double photons_to_neighboors_to_mirror= mirror_reflectivity*(exp(-length_to_mirror/att_lenght))*photons_to_neighboors/2.;
    double photons_to_neighboors_to_mirror_true=(exp(-fiber_length/att_lenght))*random->Poisson(photons_to_neighboors_to_mirror);
    double photons_to_neighboors_to_SiPM=(exp(-length_to_SiPM/att_lenght))* photons_to_neighboors/2.;

    double photons_to_neighboors_attenuated=photons_to_neighboors_to_mirror_true+photons_to_neighboors_to_SiPM;
    //cout << "photons to mirror_true "<<photons_to_neighboors_to_mirror_true<<std::endl;
    //cout << "photons to neighboor attenuation" << photons_to_neighboors_attenuated << endl;
    //Number of photon going to mirror       
    double photons_to_mirror=photons_to_SiPM; 
   
	  //taking into account attenuation lenght;
    photons_to_SiPM= (exp(-length_to_SiPM/att_lenght))* photons_to_SiPM;//450

    //Taking into account attenuation lenght and mirror  reflectivity      
    double photons_reflected=mirror_reflectivity*(exp(-length_to_mirror/att_lenght)*photons_to_mirror);

    //Taking into account fluttuation on reflectivity(poisson) and attenuation length
    double photons_reflected_true=(exp(-fiber_length/att_lenght))*random->Poisson(photons_reflected);
    
    //Total photon direct + mirror
    double photons_detected=photons_reflected_true + photons_to_SiPM;

    //Round the number of photon to get int
    unsigned int NUMBER=round(photons_detected) ; 
    unsigned int NUMBER2=round(photons_to_neighboors_attenuated) ; 

    photons =NUMBER;
    UVphotons = NUMBER2;
    //std::cout<<photons<<std::endl;          
       
  }else{
    photons=0;
    UVphotons=0;
  }
}

unsigned int SignalFiber::getNumbPhotons() const {
    return photons;
}

unsigned int SignalFiber::getNumbUVPhotons() const {
    return UVphotons;
}
double SignalFiber::getDiameter() const {
    return diameter;
}

double SignalFiber::getX() const {
    return x;
}

double SignalFiber::getY() const {
    return y;
}

int SignalFiber::round(double const& x) const {
    if((x-floor(x))<0.5){
        return floor(x);
    }
    return ceil(x);
}

void SignalFiber::Check() {
    checked=true;
}

bool SignalFiber::Checked() const {
    return checked;
}

void SignalFiber::UnCheck() {
    checked=false;
}

void SignalFiber::setNumbPhotons(unsigned int const& phot) {
    photons=phot;
}
void SignalFiber::setNumbUVPhotons(unsigned int const& UVphot) {
    UVphotons=UVphot;
}
void SignalFiber::addManyPhoton(unsigned int  newpho) {
    photons = photons+newpho;
//if(photons!=0)std::cout<<"addManyPhoton: "<<photons+newpho<<std::endl;
}

void SignalFiber::addPhoton() {
    photons = photons + 1;
}

void SignalFiber::deletePhoton() {
    if(photons==0){
        string s="You can't delete a photon because there is none";
        throw s;
    }
    photons+=-1;
}
