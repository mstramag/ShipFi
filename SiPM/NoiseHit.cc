/*
 * NoiseHit.cc
 *
 *  Created on: Dec 4, 2012
 *      Author: bruggiss
 */

#include"NoiseHit.h"
#include <cstdlib>

NoiseHit::NoiseHit(double const& t, unsigned int const& p) : time(t),pixels(p){}

NoiseHit::~NoiseHit(){
    for(unsigned int i=0;i<afterpulses.size();i++){
        delete afterpulses[i];
    }
    afterpulses.clear();
}

double NoiseHit::getTime() const {
    return time;
}

unsigned int NoiseHit::getPixels() const {
    return pixels;
}

void NoiseHit::setTime(double const& t) {
    time=t;
}

void NoiseHit::setPixels(unsigned int const& p,double const& t0, double const& recoveryT,
        double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT) {
        generateAfterpulses(0,recoveryT,prob,weightSl,tauSl,tauFa,maxT); //each time we set the pixels to a new value, the afterpulses are newly generated (but maybe with a wrong time ofset)
    pixels=p;
}

void NoiseHit::generateAfterpulses(double const& tOfset, double const& recoveryT,
            double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT) {
    /*Generate the after pulses of the noise hit.*/
    for(unsigned int i=0;i<afterpulses.size();i++){
        delete afterpulses[i];
    }
    afterpulses.clear();
    double Max=RAND_MAX;
    double r=rand()/Max;
    for(unsigned int i=0;i<pixels;i++){ //the afterpulses for all the pixels are created
        if(r<prob){ //only if there is at least one afterpulse the array is created, otherwise it will stay empty
            AfterPulses* After = new AfterPulses;
            After->generateAfterPulses(time+tOfset,recoveryT,prob,weightSl,tauSl,tauFa,maxT);
            afterpulses.push_back(After);
        }
    }
}

unsigned int NoiseHit::geteNumberOfAfterpulses() const {
    return afterpulses.size();
}

vector<AfterPulses*> NoiseHit::getAfterPulses() const {
    return afterpulses;
}
