/*
 * ShaperStehfest.h
 *
 * Created on: Jan 3, 2013
 *     Author: bruggiss
 */

#ifndef SHAPERSTEHFEST_H_
#define SHAPERSTEHFEST_H_

#include "Shaper.h"
#include <vector>

using namespace std;

class ShaperStehfest : public Shaper {
public:
    ShaperStehfest(double const& tC, double const& tS, unsigned int const& n=14);

    double OutFunction(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& t) const;
    double OutputForStandardInput(double const& t,bool const& normalized) const;

    ShaperStehfest* clone() const;

private:

    double funcInLaplace(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& s) const;
    double standardFuncInLaplace(double const& s) const;

    int factorial(int const& r) const;

    vector<double> v_i;
};

#endif /* SHAPERSTEHFEST_H_ */
