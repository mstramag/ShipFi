/*
 * Detector.h
 *
 *  Created on: Nov 30, 2012
 *      Author: bruggiss
 */

#ifndef DETECTOR_H_
#define DETECTOR_H_

#include "Channel.h"
#include "ShaperStehfest.h"
#include <vector>

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"


using namespace std;

class Detector {
public:
    Detector(unsigned int const& numofchannels,double const& fp, double const& ct,
            double const& tw, double const& iw,double const& rt,double const& q,double const& gainsigma,double const& rs,double const& rq,
            double const& cg,double const& ceq,double const& cd,double const& cq,
            double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT,double const& elect, Shaper const& shape,
            unsigned int const& numVal, double const& timeMax);
    ~Detector();

    void SimulNoiseWriteFile(unsigned int const& Events,const char * const& filename);
    void SimulNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename);
    void OneChannelNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename);
    void SimulShaperNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename);
    void PulsesWriteFile(double const& maximalTime, unsigned int const& steps, const char * const& filename); // will only write the data for one channel!
    void PlotOnePulse(const char * const& filename);

    void CheckShaper(double const& maximalTime, unsigned int const& steps, const char * const& filename, const char * const& treename, const char * const& opt);
    Channel* getChannel(unsigned int const& index) const;

    double getTimeWindow() const;
    double getIntegrationWindow() const;

    void GenerateNoise();
    void CheckAfterPulses(Channel* chan, NoiseHit* const& hit);

protected:
    unsigned int NumOfChannels;
    vector<Channel*> Channels;
    double fprimary; //the primary noise rate in Hz;
    double meanPrimary; //the probability that a pixel fires during the interval.
    double probCrossTalk;
    double timeWindow;
    double integrationWindow;

    double probAfter; //afterpulses
    double weightSlow; //afterpulses
    double tauSlow; //slow component of the afterpulses
    double tauFast; //fast component of the afterpulses
    double maxTime; //afterpulses

    double electSigma;

    TTree* settings;

    unsigned int Poisson(double const& lambda) const;
    double gaussian(double const& mean, double const& sigma, double const& x) const;
    double UncertainGain(double const& mean, double const& sigma) const;

    void Reset();
};



#endif /* DETECTOR_H_ */
