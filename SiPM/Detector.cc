/*
 * Detector.cc
 *
 *  Created on: Nov 30, 2012
 *      Author: bruggiss
 */

#include "Detector.h"
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <typeinfo>
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TFrame.h"
#include "TLegend.h"
#include "TF1.h"

//#include <random>
//#include <chrono>

Detector::Detector(unsigned int const& numofchannels, double const& fp, double const& ct,
        double const& tw, double const& iw,double const& rt,double const& q,double const& gainsigma,double const& rs,double const& rq,
        double const& cg,double const& ceq,double const& cd,double const& cq,
        double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT,double const& elect, Shaper const& shape,
        unsigned int const& numVal, double const& timeMax)
    :NumOfChannels(numofchannels),Channels(),fprimary(fp),meanPrimary(0.0),probCrossTalk(ct),timeWindow(tw),integrationWindow(iw),probAfter(prob),
     weightSlow(weightSl),tauSlow(tauSl),tauFast(tauFa),maxTime(maxT),electSigma(elect), settings(new TTree("Settings","Settings")) {
    double Rise=rt;
    double Q=q;
    double Rs=rs;
    double Rq=rq;
    double Cg=cg;
    double Ceq=ceq;
    double Cd=cd;
    double Cq=cq;
    double GainSigma=gainsigma;
    int TransType(0);
    if(typeid(shape)==typeid(ShaperFourier)) {
        TransType=1;
    }
    double CSATime=shape.getTimeCSA();
    double ShapeTime=shape.getTimeShape();
    unsigned int N_Shaper=shape.getN();
    settings->Branch("NumberOfChannels",&NumOfChannels,"NumOfChannels/I");
    settings->Branch("PrimaryNoiseFrequency",&fprimary,"fprimary/D");
    settings->Branch("ProbabilityCrossTalk",&probCrossTalk,"probCrossTalk/D");
    settings->Branch("TimeWindowForNoiseGeneration",&timeWindow,"timeWindow/D");
    settings->Branch("IntegrationWindow",&integrationWindow,"integrationWindow/D");
    settings->Branch("ProbabilityForAfterPulses",&probAfter,"probAfter/D");
    settings->Branch("WeightSlowTimeComponantAfterPulses",&weightSlow,"WeightSlow/D");
    settings->Branch("SlowTimeComponantAfterPulses",&tauSlow,"tauSlow/D");
    settings->Branch("FastTimeComponantAfterPulses",&tauFast,"tauFast/D");
    settings->Branch("MaximalTimeAfterPulses",&maxTime,"maxTime/D");
    settings->Branch("SigmaElectonicNoise",&electSigma,"electSigma/D");
    settings->Branch("GainSigma",&GainSigma,"GainSigma/D");
    settings->Branch("SignalRiseTime",&Rise,"Rise/D");
    settings->Branch("ChargeForOnePulse",&Q,"Q/D");
    settings->Branch("ResesistanceRs",&Rs,"Rs/D");
    settings->Branch("ResistanceRq",&Rq,"Rq/D");
    settings->Branch("CapacitanceCg",&Cg,"Cg/D");
    settings->Branch("CapacitanceCeq",&Ceq,"Ceq/D");
    settings->Branch("CapacitanceCd",&Cd,"Cd/D");
    settings->Branch("CapacitanceCq",&Cq,"Cq/D");
    settings->Branch("LaplaceTranformType",&TransType,"TransType/I");
    settings->Branch("CSATime",&CSATime,"CSATime/D");
    settings->Branch("ShapeTime",&ShapeTime,"ShapeTime/D");
    settings->Branch("InverseLaplaceStepsN",&N_Shaper,"N_Shaper/I");
    if(typeid(shape)==typeid(ShaperFourier)) {
        double a=shape.geta();
        settings->Branch("InverseLaplaceA",&a,"a/D");
    }
    Shaper* shaper = shape.clone();
    shaper->initialize(rt,rs,rq,cg,ceq,cd,cq,numVal,timeMax);
    for(unsigned int i=0;i<NumOfChannels;++i) {
        Channel* Chan = new Channel(rt,UncertainGain(q,gainsigma),rs,rq,cg,ceq,cd,cq,shaper,numVal,timeMax);
        Channels.push_back(Chan);
    }
    meanPrimary=timeWindow*fprimary;
    settings->GetBranch("NumberOfChannels")->Fill();
    settings->GetBranch("PrimaryNoiseFrequency")->Fill();
    settings->GetBranch("ProbabilityCrossTalk")->Fill();
    settings->GetBranch("TimeWindowForNoiseGeneration")->Fill();
    settings->GetBranch("IntegrationWindow")->Fill();
    settings->GetBranch("ProbabilityForAfterPulses")->Fill();
    settings->GetBranch("WeightSlowTimeComponantAfterPulses")->Fill();
    settings->GetBranch("SlowTimeComponantAfterPulses")->Fill();
    settings->GetBranch("FastTimeComponantAfterPulses")->Fill();
    settings->GetBranch("MaximalTimeAfterPulses")->Fill();
    settings->GetBranch("SigmaElectonicNoise")->Fill();
    settings->GetBranch("GainSigma")->Fill();
    settings->GetBranch("SignalRiseTime")->Fill();
    settings->GetBranch("ChargeForOnePulse")->Fill();
    settings->GetBranch("ResesistanceRs")->Fill();
    settings->GetBranch("ResistanceRq")->Fill();
    settings->GetBranch("CapacitanceCg")->Fill();
    settings->GetBranch("CapacitanceCeq")->Fill();
    settings->GetBranch("CapacitanceCd")->Fill();
    settings->GetBranch("CapacitanceCq")->Fill();
    settings->GetBranch("LaplaceTranformType")->Fill();
    settings->GetBranch("CSATime")->Fill();
    settings->GetBranch("ShapeTime")->Fill();
    settings->GetBranch("InverseLaplaceStepsN")->Fill();
    if(typeid(shape)==typeid(ShaperFourier)) {
	settings->GetBranch("InverseLaplaceA")->Fill();
    }
}

Detector::~Detector() {
    for(unsigned int i=0;i<NumOfChannels;++i) {
        //cerr << "Channel deleted" << endl;
            delete Channels[i];
        }
}

Channel* Detector::getChannel(unsigned int const& index) const {
    return Channels[index];
}

double Detector::getTimeWindow() const {
    return timeWindow;
}

double Detector::getIntegrationWindow() const {
    return integrationWindow;
}

void Detector::GenerateNoise() {
    /*Fill all the channels with the noise.
     * Generate a random number of primary noise hits, following a poisson distribution with a mean that is calculated from the user-given primary noise frequency.
     * */
    double Max=RAND_MAX;
    //Loop over all channels
    for(unsigned int i=0;i<NumOfChannels;++i){
    //For each channel we generate a certain number of hits following a Poisson distribution with mean (freq (per channel)* timewindow)    
        unsigned int hits=Poisson(meanPrimary);

     //These hits are "transformed" in Noisehits by giving them a random time in the time window and a certain number of pixels (cross-talk of these noise hits)   
        for(unsigned int j=1;j<=hits;j++){
            double t=rand()/Max*timeWindow;
            double u=rand()/Max;
            unsigned int num(0);
            do {
            u=rand()/Max;
            num++;
            } while(u<probCrossTalk);
            NoiseHit* hit = new NoiseHit(t,num);
            //Generates after-pulses from these hits
            hit->generateAfterpulses(Channels[i]->getRiseTime(),Channels[i]->getTauR(),probAfter,weightSlow,tauSlow,tauFast,maxTime);
            CheckAfterPulses(Channels[i],hit);
            //Add the hits in the channel
            Channels[i]->addNoiseHit(hit);
        //    cerr << "j = " << j << endl;
        }
    }
}

void Detector::CheckAfterPulses(Channel* chan, NoiseHit* const& hit) {
    double Max=RAND_MAX;
    for(unsigned int k=0;k<hit->geteNumberOfAfterpulses();k++){
        for(unsigned int i=0;i<hit->getAfterPulses()[k]->getTime().size();i++){
            double tAfter=hit->getAfterPulses()[k]->getTime()[i];
            double intensAfter=hit->getAfterPulses()[k]->getIntensity()[i];
            unsigned int numAfter(0);
            double r=rand()/Max;
        //    cerr << "i = " << i << endl;
        //    cerr << "k = " << k << endl;
            while(r<intensAfter*probCrossTalk){
                r=rand()/Max;
                numAfter++;
            }
            if(numAfter>0){
                NoiseHit* hitAfter = new NoiseHit(tAfter,numAfter);
                hitAfter->generateAfterpulses(chan->getRiseTime(),chan->getTauR(),probAfter,weightSlow,tauSlow,tauFast,maxTime);
                CheckAfterPulses(chan,hitAfter);
                chan->addNoiseHit(hitAfter);
            }
        }
    }
}


void Detector::SimulNoiseWriteFile(unsigned int const& Events,const char * const& filename) {
    ofstream OutputFile;
    OutputFile.open(filename);
    for(unsigned int i=0;i<Events;i++){
        GenerateNoise();
        OutputFile << Channels[0]->ADCs(timeWindow,integrationWindow,electSigma);
        for(unsigned int j=1;j<NumOfChannels;j++){
            OutputFile  << " " << Channels[j]->ADCs(timeWindow,integrationWindow,electSigma);
        }
        OutputFile << endl;
        Reset();
        if(i%1000==0) {
            cerr << i << endl;
        }
    }
    OutputFile.close();
}


void Detector::SimulNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename){
    cerr << endl;
    cerr << endl;
    for(unsigned int j=0;j<100;j++){
        cerr << "#";
    }
    cerr << endl;
    cerr << "Noise simulation in course" << endl;
    TFile* file = new TFile(filename,"UPDATE");
    stringstream result;
    result << treename;
    result << "Result";
    string res=result.str();
    const char* r=res.c_str();
    TTree* tree = new TTree(r,r);

    stringstream setting;
    setting << treename;
    setting << "Settings";
    string set=setting.str();
    const char* s=set.c_str();
    settings->SetName(s);
    unsigned int events=Events;
    settings->Branch("NumberOfEvents",&events,"events/I");
//    settings->GetBranch("NumberOfEvents")->Fill();
    settings->Fill();

    Short_t L1_S1[1600];
    tree->Branch("L1_S1",&L1_S1,"L1_S1[1600]/S");
    int bar=0;
    for(unsigned int i=0;i<Events;i++){
        GenerateNoise();
        for(unsigned int j=0;j<NumOfChannels;j++){
            L1_S1[j] = Channels[j]->ADCs(timeWindow,integrationWindow,electSigma);
        }
        tree->Fill();
        Reset();
        if(i%(Events/100)==0 && bar<100) {
            cerr << "#";
            bar++;
        }
    }
    //tree->Write();
    file->Write();
    settings->Write();
    file->Close();
    cerr << endl;
}

void Detector::OneChannelNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename){
    cerr << endl;
    cerr << endl;
    for(unsigned int j=0;j<100;j++){
        cerr << "#";
    }
    cerr << endl;
    cerr << "Noise simulation in course" << endl;
    TFile* file = new TFile(filename,"RECREATE");
    stringstream result;
    result << treename;
    result << "NoiseData";
    string res=result.str();
    const char* r=res.c_str();
    TTree* tree = new TTree(r,r);

    stringstream setting;
    setting << treename;
    setting << "Settings";
    string set=setting.str();
    const char* s=set.c_str();
    settings->SetName(s);
    unsigned int events=Events;
    settings->Branch("NumberOfEvents",&events,"events/I");
//    settings->GetBranch("NumberOfEvents")->Fill();
    settings->Fill();

    Short_t L1_S1;
    int nNoiseHit;
    double timeNoise[nNoiseHit];
    int npixelNoise[nNoiseHit];
    tree->Branch("L1_S1",&L1_S1,"L1_S1/S");
    tree->Branch("nNoiseHit",&nNoiseHit,"nNoiseHit/I");
    tree->Branch("timeNoise",&timeNoise,"timeNoise[nNoiseHit]/D");
    tree->Branch("npixelNoise",&npixelNoise,"npixelNoise[nNoiseHit]/I");
    int bar=0;
    for(unsigned int i=0;i<Events;i++){
        GenerateNoise();
        L1_S1 = Channels[0]->ADCs(timeWindow,integrationWindow,electSigma);
        nNoiseHit = Channels[0]->getHitsNumber();
        for(int j=0; j<nNoiseHit; j++){
            NoiseHit* hit = Channels[0]->getNoiseHits(j);
            timeNoise[j] = hit->getTime();
            npixelNoise[j] = hit->getPixels();
        }
        tree->Fill();
        Reset();
        if(i%(Events/100)==0 && bar<100) {
            cerr << "#";
            bar++;
        }
    }
    //tree->Write();
    file->Write();
    settings->Write();
    file->Close();
    cerr << endl;
}

void Detector::SimulShaperNoiseWriteRootFile(unsigned int const& Events,const char * const& filename,const char* const& treename) {
    cerr << endl;
    cerr << endl;
    for(unsigned int j=0;j<100;j++){
        cerr << "#";
    }
    cerr << endl;
    cerr << "Noise simulation in course" << endl;
    TFile* file = new TFile(filename,"UPDATE");
    stringstream result;
    result << treename;
    result << "Result";
    string res=result.str();
    const char* r=res.c_str();
    TTree* tree = new TTree(r,r);

    stringstream setting;
    setting << treename;
    setting << "Settings";
    string set=setting.str();
    const char* s=set.c_str();
    settings->SetName(s);
    unsigned int events=Events;
    settings->Branch("NumberOfEvents",&events,"events/I");
//    settings->GetBranch("NumberOfEvents")->Fill();

    Short_t L1_S1[1600];
    tree->Branch("L1_S1",&L1_S1,"L1_S1[1600]/S");
    int bar=0;
    double dcr(0.0);
    for(unsigned int i=0;i<Events;i++){
        GenerateNoise();
        for(unsigned int j=0;j<NumOfChannels;j++){
            L1_S1[j] = Channels[j]->ShaperNoiseADCs(timeWindow,integrationWindow,electSigma);
            dcr+=Channels[j]->DCR(timeWindow,integrationWindow);
        }
        tree->Fill();
        Reset();
        if(i%(Events/100)==0 && bar<100) {
            cerr << "#";
            bar++;
        }
    }
    dcr=dcr/(NumOfChannels*Events);
    settings->Branch("MeanDCR",&dcr,"dcr/D");
//    settings->GetBranch("MeanDCR")->Fill();
    //tree->Write();
    settings->Fill();
    file->Write();
    settings->Write();
    file->Close();
    cerr << endl;
    cerr << endl;
    cerr << endl;
}

void Detector::PulsesWriteFile(double const& maximalTime, unsigned int const& steps, const char * const& filename) {
    ofstream OutputFile;
    OutputFile.open(filename);
    double dt=maximalTime/steps;
    do
        GenerateNoise();
    while(Channels[0]->getHitsNumber()==0);
    for(unsigned int i=1;i<Channels[0]->getHitsNumber();i++){
        //cerr << i << endl;
        double t=Channels[0]->getNoiseHits(i)->getTime();
        double p=Channels[0]->getNoiseHits(i)->getPixels();
    //    cerr << t << endl;
    //    cerr << p << endl;
        for(unsigned int j=0;j<steps;j++){
            OutputFile << t+j*dt << " " << Channels[0]->OutCurrent(j*dt,p) << " 0" << endl;
        }
        for(unsigned int k=0;k<Channels[0]->getNoiseHits(i)->geteNumberOfAfterpulses();k++){
            vector<double> afterTime=Channels[0]->getNoiseHits(i)->getAfterPulses()[k]->getTime();
            vector<double> afterIntens=Channels[0]->getNoiseHits(i)->getAfterPulses()[k]->getIntensity();
            for(unsigned int k=0;k<afterTime.size();k++){
                for(unsigned int j=0;j<steps;j++){
                    OutputFile << afterTime[k]+j*dt << " " << Channels[0]->OutCurrent(j*dt,afterIntens[k]) << " 1" << endl;
                }
            }
        }
    }
    OutputFile.close();
}

void Detector::PlotOnePulse(const char* const& filename) {
    TF1* pulse = new TF1("pulse",Channels[0],&Channel::OutCurrentForPulseShape,-10e-9,100e-9,0);
    TCanvas* c1 = new TCanvas("DetectorPulse","DetectorPulse");
    pulse->Draw();
    pulse->GetXaxis()->SetTitle("Time [sec]");
    pulse->GetYaxis()->SetTitle("Current [arbitrary units]");
    pulse->GetYaxis()->SetTitleOffset(1.15);
    c1->SaveAs(filename);
}

/*void Detector::CheckShaper(double const& maximalTime, unsigned int const& steps, const char * const& filename) {
    ofstream OutputFile;
    OutputFile.open(filename);
    double dt=maximalTime/steps;

    for(unsigned int j=0;j<steps;j++){
        OutputFile << j*dt << " " << Channels[0]->ShaperOut(j*dt) << endl;
    }
    OutputFile.close();
}*/

void Detector::CheckShaper(double const& maximalTime, unsigned int const& steps, const char * const& filename, const char * const& treename, const char * const& opt) {
    stringstream eps;
    eps << filename;
    eps << ".pdf";
    string EPS=eps.str();
    const char* epsFile=EPS.c_str();

    stringstream root;
    root << filename;
    root << ".root";
    string ROOT=root.str();
    const char* rootFile=ROOT.c_str();
    TFile* file = new TFile(rootFile,opt);

    stringstream Canv;
    Canv << treename;
    Canv << "Plots";
    string CANV=Canv.str();
    const char* canv=CANV.c_str();

    stringstream tree;
    tree << treename;
    tree << "Result";
    string Tree=tree.str();
    const char* r=Tree.c_str();

    stringstream gr;
    gr << treename;
    gr << "Histo";
    string Gr=gr.str();
    const char* histo=Gr.c_str();

    stringstream set;
    set << treename;
    set << "Settings";
    string Set=set.str();
    const char* s=Set.c_str();

    settings->SetName(s);
    Double_t MT=maximalTime;
    Short_t STEPS=steps;
    settings->Branch("MaximalTime",&MT,"MT/D");
    settings->Branch("Steps",&STEPS,"STEPS/S");
//    settings->GetBranch("MaximalTime")->Fill();
//    settings->GetBranch("Steps")->Fill();
    settings->Fill();
    double dt=maximalTime/(steps);

    TH1* hist = new TH1D(histo,histo,steps,dt/2.0,maximalTime+dt/2.0);

    TTree* TREE = new TTree(r,r);
    Double_t Time;
    Double_t Current;
    TREE->Branch("Time",&Time,"Time/D");
    TREE->Branch("Current",&Current,"Current/D");

    Double_t time[steps];
    Double_t currentStandard[steps];
    Double_t currentDetector[steps];
    Double_t currentAnalytical[steps];
    Double_t currentDiscrete[steps];
    for(unsigned int j=1;j<=steps;j++){
        Time = j*dt;
        time[j-1] = j*dt*1e9;
        currentStandard[j-1] = Channels[0]->ShaperStandardOut(j*dt);
        Current = Channels[0]->ShaperStandardOut(j*dt);
        currentDetector[j-1] = Channels[0]->ShaperOut(j*dt)/Channels[0]->getShaper()->getMax();
        currentAnalytical[j-1] = Channels[0]->AnalyticalStandardShaper(j*dt);
        currentDiscrete[j-1]=Channels[0]->DiscreteOutput(j*dt,true);
        TREE->Fill();
        hist->Fill(Time,Current);
    }
    int bin1 = hist->FindFirstBinAbove(hist->GetMaximum()/2);
    int bin2 = hist->FindLastBinAbove(hist->GetMaximum()/2);
    double fwhm = hist->GetBinCenter(bin2) - hist->GetBinCenter(bin1);
    cout<<"Shaping time (FWHM): "<<fwhm*1e9/2.4<<"ns ("<<fwhm*1e9<<" ns)"<<endl;
    int binmax = hist->GetMaximumBin();
    int bin0   = hist->FindFirstBinAbove(0);
    double Tp = hist->GetBinCenter(binmax);
    double T0 = hist->GetBinCenter(bin0);
    cout<<"Peaking time: "<<Tp*1e9-T0*1e9<<"ns"<<endl;
    TCanvas *c1 = new TCanvas(canv,"Shaper",200,10,700,500);
    c1->SetGridx();
    c1->Divide(2,1);
    c1->cd(1);
    TGraph* grStandard = new TGraph(steps,time,currentStandard);
    //grStandard->SetTitle("Standard Input");
    TGraph* grAnalytical = new TGraph(steps,time,currentAnalytical);
    grAnalytical->SetMarkerColor(2);
    grAnalytical->SetLineColor(2);
    TMultiGraph *mg = new TMultiGraph();
    mg->SetTitle("Standard Input");
    mg->Add(grAnalytical);
    mg->Add(grStandard);
    mg->Draw("ACP");
    mg->GetXaxis()->SetTitle("Time / ns");
    mg->GetYaxis()->SetTitle("Current / normalized units");
    mg->GetYaxis()->SetTitleOffset(1.45);
    mg->GetYaxis()->SetLabelSize(0.03);
    TLegend* leg1 = new TLegend(0.35,0.8,0.9,0.9);
    leg1->AddEntry(grAnalytical,"Analytical inverse Laplace transform","l");
    leg1->AddEntry(grStandard,"Numeric inverse Laplace transform","l");
    leg1->SetTextSize(0.025);
    leg1->Draw();

    c1->cd(2);
    TGraph* grDetector = new TGraph(steps,time,currentDetector);
    grDetector->SetMarkerColor(2);
    grDetector->SetLineColor(2);
    TGraph* grDiscrete = new TGraph(steps,time,currentDiscrete);
    TMultiGraph *mg2 = new TMultiGraph();
    mg2->SetTitle("Detector response");
    mg2->Add(grDetector);
    mg2->Add(grDiscrete);
    mg2->Draw("ACP");
    mg2->GetXaxis()->SetTitle("Time / ns");
    mg2->GetYaxis()->SetTitle("Current / normalized units");
    mg2->GetYaxis()->SetTitleOffset(1.45);
    mg2->GetYaxis()->SetLabelSize(0.03);
    TLegend* leg2 = new TLegend(0.4,0.8,0.9,0.9);
    leg2->AddEntry(grDetector,"Numeric inverse Laplace transform","l");
    leg2->AddEntry(grDiscrete,"Lookup-table Laplace transform","l");
    leg2->SetTextSize(0.025);
    leg2->Draw();
    c1->Update();
    c1->Modified();
    c1->SaveAs(epsFile);
    c1->Write();
    //hist->Write();
    //TREE->Write();
    file->Write();
    settings->Write();
    file->Close();
}

/*void Detector::CheckStandartShaperError(double const& inpParam, double const& maximalTime, unsigned int const& steps, const char * const& filename) {
    ofstream OutputFile;
        OutputFile.open(filename);
        double dt=maximalTime/steps;
            for(unsigned int j=0;j<steps;j++){
                OutputFile << j*dt << " " << Channels[0]->ShaperStandardOut(inpParam,j*dt) << " " << Channels[0]->AnalyticalStandartShaper(inpParam,j*dt) << endl;
            }
        OutputFile.close();
}*/

void Detector::Reset() {
    for(unsigned int i=0;i<NumOfChannels;++i) {
        //cerr << "Channel deleted in reset()" << endl;
        Channels[i]->ResetHits();
        }
}

unsigned int Detector::Poisson(double const& lambda) const {
    double Max=RAND_MAX;
    double L=exp(-lambda);
    unsigned int k=0;
    double p(1.0);
    double u(0.0);
    do {
        k++;
        u=rand()/Max;
        p*=u;
    } while(p>L);
    return k-1;
}

double Detector::UncertainGain(double const& mean, double const& sigma) const{
    double Max=RAND_MAX;
    double Range=10*sigma;
    double c=1/(sigma*sqrt(2*3.14159265))*Range;
    double x=(rand()/Max-0.5)*Range+mean;
    double u=rand()/Max;
    while(c*1/Range*u>gaussian(mean,sigma,x)){
        x=(rand()/Max-0.5)*Range+mean;
        u=rand()/Max;
    }
    return x;
}

double Detector::gaussian(double const& mean, double const& sigma, double const& x) const{
    return 1/(sigma*sqrt(2*3.14159265))*exp(-0.5*((x-mean)/sigma)*((x-mean)/sigma));
}
