/*
 * ShaperFourier.h
 *
 *  Created on: Jan 7, 2013
 *      Author: bruggiss
 */

#ifndef SHAPERFOURIER_H_
#define SHAPERFOURIER_H_

#include "Shaper.h"
#include <complex>

 #ifndef M_Pi 
#define M_PI 3.1415926535897932384626433832795
#endif

using namespace std;

class ShaperFourier : public Shaper {
public:
    ShaperFourier(double const& tC, double const& tS, unsigned int const& n=100, double const& A=4.5);

    double OutFunction(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& t) const;
    double OutputForStandardInput(double const& t,bool const& normalized) const;
    double geta() const;

    ShaperFourier* clone() const;

private:

    complex<double> funcInLaplace(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, complex<double> const& s) const;
    complex<double> standardFuncInLaplace(complex<double> const& s) const;

    double a;
};



#endif /* SHAPERFOURIER_H_ */
