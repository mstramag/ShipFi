/*
 * ShaperFourier.cc
 *
 *  Created on: Jan 7, 2013
 *      Author: bruggiss
 */


#include "ShaperFourier.h"
#include <cmath>
#include <complex>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>


ShaperFourier::ShaperFourier(double const& tC, double const& tS, unsigned int const& n, double const& A) : Shaper(tC,tS,n), a(A){}

ShaperFourier* ShaperFourier::clone() const {
    return new ShaperFourier(*this);
}

double ShaperFourier::geta() const {
    /*****
     * a: constant in the Fourier series method
     * */
    return a;
}

double ShaperFourier::OutFunction(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& t) const {
    /*******The Fourier series method approximates the inversion integral
     * using the following equation:
     *f(t) = exp(at)/t*[0.5*F(a)+ReSum_k=1^n F(a+j*k*pi/t)*(-1)^k]
     *j=sqrt(-1)
     *The parameters a and n must be optimized for increased accuracy.
     *Please check A and N in .sh file for a and n.
     * *******************************************
     * the output function of the shaper:
     *Output(t) = Ls^-1[TF(s)*Lt[DetResp(t)](s)](t)
     * ****/
    complex<double> intermediate(0.0,0.0);
    double b=a/t;
    for(unsigned int i=1;i<=N;i++){
        double k=i;
        complex<double> j(0,1);
        intermediate+=funcInLaplace(tauQ,tauIn,tauR,riseTime,b+j*k*M_PI/t)*pow(-1,k);
    }
    double result=exp(b*t)/t*(0.5*real(funcInLaplace(tauQ,tauIn,tauR,riseTime,b))+real(intermediate));
    if(result!=result) {
        stringstream s;
        s << "Error in the Laplace transform: a NaN value result at t = " << t ;
        string str=s.str();
        throw str;
    }
    return result;
}

double ShaperFourier::OutputForStandardInput(double const& t,bool const& normalized) const {
    /*******
     *Ls^-1[TF(s)](t)
     * ****/
    complex<double> intermediate(0.0,0.0);
    double b=a/t;
    for(unsigned int i=1;i<=N;i++){
        double k=i;
        complex<double> j(0,1);
        intermediate+=standardFuncInLaplace(b+j*k*M_PI/t)*pow(-1,k);
    }
    double result=exp(b*t)/t*(0.5*real(standardFuncInLaplace(b))+real(intermediate));
    if(result!=result) {
        stringstream s;
        s << "Error in the Laplace transform: a NaN value result at t = " << t ;
        string str=s.str();
        throw str;
    }
    if(normalized){
        return result/maxStandard;
    }
    return result;
}


complex<double> ShaperFourier::funcInLaplace(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, complex<double> const& s) const {
    /*****
     *TF(s)*Lt[DetResp(t)](s)
     * **/
    complex<double> rise=((tauQ/(tauIn*tauR))/riseTime)*exp(-riseTime*s)*(-riseTime*s+exp(riseTime*s)-1.0)/(s*s);
    complex<double> a=1.0/(tauR-tauIn);
    complex<double> b=(tauQ-tauIn)/tauIn;
    complex<double> c=tauIn;
    complex<double> d=(tauR-tauQ)/tauR;
    complex<double> e=tauR;
    complex<double> fall=a*(b*c*exp(-riseTime*s)/(c*s+1.0)+d*e*exp(-riseTime*s)/(e*s+1.0));
    return (rise+fall)*standardFuncInLaplace(s);
}

complex<double> ShaperFourier::standardFuncInLaplace(complex<double> const& s) const {
    /*****
     *TF(s) = 1/(1+s*tauCSA)*1/(1+s*tauShaper)^2
     * **/
    complex<double> csa=1.0/(1.0+s*timeCSA)/**pow(s,1.0/25.0)*/; //create undershoot
    complex<double> shape=1.0/(pow((1.0+s*timeShape),2.0));
    return (1.0/*+0.0*exp(-10.0*timeCSA*s)*/)*csa*shape;    //second delta injection after 10.0*timeCSA
}


