/*
 * SuperDetector.h
 *
 *  Created on: Jan 22, 2013
 *      Author: bruggis
 */

#ifndef SUPERDETECTOR_H_
#define SUPERDETECTOR_H_

#include "Detector.h"
#include "SignalSetup.h"
#include "Shaper.h"
#include "ShaperStehfest.h"
#include "ShaperFourier.h"
#include "AfterPulses.h"
#include "NoiseHit.h"
#include "Channel.h"
#include "SignalFiber.h"
#include "SignalSetup.h"
#include "SignalChannels.h"

#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TLatex.h"

struct Layer {
    double angle; //(rad) angle between layer and y axis (assume layer is a //ogram 2 sides // to x axis and two sides with same angle wrt y axis
    double width; //(mm) dimension on x axis (one edge, not projection)
    double length; //(mm) dimension on y axis
    double x; //(mm) center of the layer on the x axis 
    double y; //(mm) center of the layer on the y axis
    double z; //(mm) z coordinate of the layer (assume the layer is parallel to xy plane
};

using namespace std;

class SuperDetector : public Detector {
public:
    
    Int_t           m_Event;
    vector<double>  m_x;
    vector<double>  m_y;
    vector<double>  m_z;
    vector<double>  m_thetaangle;
    vector<double>  m_phiangle;
    vector<double>  m_Ptot;
    vector<double>  m_PdgID;
    std::vector<double> L1_S1;

    SuperDetector(double const& fp, double const& ct,
            double const& tw, double const& iw,double const& rt,double const& q,double const& gainsigma,double const& rs,double const& rq,
            double const& cg,double const& ceq,double const& cd,double const& cq,
            double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT,double const& elect, Shaper const& shape,
            unsigned int const& numVal, double const& timeMax,double const& diam, double const& diamSigma, double const& gap, double const& posSigma,
            double const& channelWidth, double const& channelHight, unsigned int const& numbOfDistr,double const& QE, double const& epoxy, unsigned int const& layers,unsigned int const& nfibers,double const& fiber_length, 
            double const& brewster, double const& reflectivity, double const& attlen,
		  double const& fxt, double const& dfxt);

    void generateDataWriteROOT(double const& thetaMax,unsigned int const& meanPhotons, double const& sigma,unsigned int const& spareChannels, double const& probFiberXTalk, unsigned int const& Rand, unsigned int const& method,unsigned const& layers, unsigned const& nfibers,double const& fiber_length, double const& fxt,double const& dfxt, double const& angle_cut, std::vector<double> x,std::vector<double> y,std::vector<double> z,std::vector<double> thetaangle,std::vector<double> phiangle,std::vector<double> Ptot,std::vector<double> PdgID,std::vector<double>& ch,std::vector<std::vector<double>>& L1_S1_N,std::vector<std::vector<double>>& L1_S1_S,std::vector<std::vector<double>>& L1_S1_P,std::vector<std::vector<double>>& L1_S1_D);
   private:
    SignalSetup* signal_setup;
    TRandom3* random;
    vector<double> thetaIntersept;
    vector<double> phiIntersept;
    vector<double> thetaxIntersept;
    vector<double> thetayIntersept;
    vector<double> chIntersept;
    vector<Double_t> x0Intersept;
    vector<Layer> Layers;
    void GenerateSignal(double const& thetaMax,unsigned int const& meanPhotons, double const& sigma, unsigned int const& spareChannels,
			double const& probFiberxTalk, unsigned int const& rand, unsigned int const& method, unsigned const& layers, unsigned const& nfibers,double const& fiber_length, double const& fxt, double const& dfxt, double const& angle_cut, int i);

    void CheckSignalAfterPulses(Channel* chan, NoiseHit* const& hit);
    bool TrackInAllLayers(double const& thetax, double const& thetay, int i=0); //assumes the track goes through the first layer in layers

};


#endif /* SUPERDETECTOR_H_ */
