/*
 * Channel.cc
 *
 *  Created on: Nov 30, 2012
 *      Author: bruggiss
 */

#include "Channel.h"
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <string>

Channel::Channel(double const& rt,double const& q,double const& rs,double const& rq,
        double const& cg,double const& ceq,double const& cd,double const& cq,Shaper* const& shape, unsigned int const& numVal, double const& timeMax)
        : RiseTime(rt),Q(q),Rs(rs),Rq(rq),Cg(cg),Ceq(ceq),
        Cd(cd),Cq(cq){
    shaper = shape->clone();
    //shaper->initialize(getTauQ(),getTauIn(),getTauR(),RiseTime,numVal,timeMax);
}


Channel::~Channel() {
    for(unsigned int i=0;i<hits.size();++i){
        delete hits[i];
    }
}

Channel* Channel::Copy() const {
    Channel* Chan = new Channel(*this);
    return Chan;
}


double Channel::getTauIn() const {
    return Rs*(Cg+Ceq);
}

double Channel::getTauR() const {
    return     Rq*(Cd+Cq);
}

double Channel::getTauQ() const{
    return Rq*Cq;
}

Shaper* Channel::getShaper() const{
    return shaper;
}

double Channel::OutCurrent(double const& t,double const& intens) const {
    if(t-RiseTime<0){
        return ((Q*getTauQ()/(getTauIn()*getTauR()))*t/RiseTime)*intens/(1.0+0.5*getTauQ()/(getTauIn()*getTauR())*(RiseTime));
        //return 0;
    }else {
        return (Q/(getTauR()-getTauIn())*((((getTauQ()-getTauIn())/getTauIn())*
                exp(-(t-RiseTime)/getTauIn()))+(((getTauR()-getTauQ())/getTauR())*
                        exp(-(t-RiseTime)/getTauR()))))*intens/(1.0+0.5*getTauQ()/(getTauIn()*getTauR())*(RiseTime));
    }
}

double Channel::OutCurrentForPulseShape(double* x, double* p) {
    if(*x<0.0){
        return 0.0;
    }
    if(*x-RiseTime<0){
        return ((Q*getTauQ()/(getTauIn()*getTauR()))*(*x)/RiseTime)/(1.0+0.5*getTauQ()/(getTauIn()*getTauR())*(RiseTime));
        //return 0;
    }else {
        return (Q/(getTauR()-getTauIn())*((((getTauQ()-getTauIn())/getTauIn())*
                exp(-(*x-RiseTime)/getTauIn()))+(((getTauR()-getTauQ())/getTauR())*
                        exp(-(*x-RiseTime)/getTauR()))))/(1.0+0.5*getTauQ()/(getTauIn()*getTauR())*(RiseTime));
    }
}

double Channel::ShaperOut(double const& t) const {
    double res(0.0);
    try{
    res = shaper->OutFunction(getTauQ(),getTauIn(),getTauR(),RiseTime,t);
    } catch(string& s){
        cout << s << endl;
    }
    return res;
}

double Channel::ShaperStandardOut(double const& t) const {
    double res(0.0);
    try{
    res = shaper->OutputForStandardInput(t,true);
    } catch(string& s){
        cout << s << endl;
    }
    return res;
}

double Channel::DiscreteOutput(double const& t, bool const& normalized) const {
    return shaper->DiscreteOutFunction(t,normalized);
}

double Channel::AnalyticalStandardShaper(double const& t) const {
    double r=shaper->getTimeCSA();
    double q=shaper->getTimeShape();
    if(r==q){
        return t*t*exp(-t/r)/(2.0*r*r*r)/shaper->getMaxStandard();
    }
    return (exp(-t/q-t/r)*(q*r*exp(t/q)-q*r*exp(t/r)+q*t*exp(t/r)-r*t*exp(t/r))/(q*(q-r)*(q-r)))/shaper->getMaxStandard();
}

int Channel::ADCs(double const& timeWindow,double const& integrationWindow,double const& electNoiseSigma) const {
    //ADC is the integral of the current of the noise hits inside the integration window
    double integral(0.0);
//    stringstream s;
    for(unsigned int i=0; i<hits.size();i++){
        integral+=integrateCurrent(hits[i]->getTime(),hits[i]->getPixels(),timeWindow,integrationWindow);
        //cout << "Pixels = " << hits[i]->getPixels() << endl;
        //cout << "Pixels with afterpulses = " << hits[i]->geteNumberOfAfterpulses() << endl;
        //cout << "Contribution of this pixel = " << round(integrateCurrent(hits[i]->getTime(),hits[i]->getPixels(),timeWindow,integrationWindow)) << endl;
        for(unsigned int k=0;k<hits[i]->geteNumberOfAfterpulses();k++){
            vector<double> afterTime=hits[i]->getAfterPulses()[k]->getTime();
            vector<double> afterIntens=hits[i]->getAfterPulses()[k]->getIntensity();
          //  cout << "Total number of after pulses for this pixel = " << afterTime.size() << endl;
            for(unsigned int j=0;j<afterTime.size();j++){
            //    cout << "Intensity of the after pulses = " << afterIntens[j] << endl;
             //   cout << "Time delay of the after pulse = " << afterTime[j]-hits[i]->getTime() << endl;
              //  cout << "Contribution of this pixel = " << round(integrateCurrent(afterTime[j],afterIntens[j],timeWindow,integrationWindow)) << endl;
                integral+=integrateCurrent(afterTime[j],afterIntens[j],timeWindow,integrationWindow);
            }
        }
    }
 //   std::cout<<integral<<std::endl;
    double ADC = round(ElectronicNoise(integral,electNoiseSigma));
    if(ADC>32767){
    //    cerr << s.str();
        cerr << "Too high values" << endl;
        cerr << endl;
        return 32767;
    }
    //cerr << "end" << endl;
    return ADC;
}

double Channel::DCR(double const& timeWindow, double const& integrationWindow) const{
    //Return the mean number of pixels fired inside the timeWindow
    double dcr(0.0);
//    stringstream s;
    for(unsigned int i=0; i<hits.size();i++){
        if(hits[i]->getTime()>=(timeWindow-integrationWindow) && hits[i]->getTime()<=timeWindow){
            dcr+=hits[i]->getPixels();
        }
        for(unsigned int k=0;k<hits[i]->geteNumberOfAfterpulses();k++){
            vector<double> afterTime=hits[i]->getAfterPulses()[k]->getTime();
            vector<double> afterIntens=hits[i]->getAfterPulses()[k]->getIntensity();
            for(unsigned int j=0;j<afterTime.size();j++){
                if(afterTime[j]>=(timeWindow-integrationWindow) && afterTime[j]<=timeWindow){
                    dcr+=afterIntens[j];
                }
            }
        }
    }
    return dcr/integrationWindow;
}

int Channel::ShaperNoiseADCs(double const& timeWindow, double const& integrationWindow, double const& electNoiseSigma) const {
    /*The result of the discretisized output of the shaper.*/
    double integral(0.0);
    for(unsigned int i=0; i<hits.size();i++){
        //integral+=DiscreteOutput(timeWindow-integrationWindow-hits[i]->getTime(),true)*hits[i]->getPixels()*Q;
        //cout << "Pixels = " << hits[i]->getPixels() <<" Q: "<<Q<< endl;
        //cout << "Contribution of this pixel = " << round(integrateCurrent(hits[i]->getTime(),hits[i]->getPixels(),timeWindow,integrationWindow)) << endl;
        integral+=DiscreteOutput(-1*timeWindow+integrationWindow+hits[i]->getTime(),true)*hits[i]->getPixels()*Q;
        for(unsigned int k=0;k<hits[i]->geteNumberOfAfterpulses();k++){
            vector<double> afterTime=hits[i]->getAfterPulses()[k]->getTime();
            vector<double> afterIntens=hits[i]->getAfterPulses()[k]->getIntensity();
            for(unsigned int j=0;j<afterTime.size();j++){
                //integral+=DiscreteOutput(timeWindow-integrationWindow-afterTime[j],true)*afterIntens[j]*Q;
                integral+=DiscreteOutput(-1*timeWindow+integrationWindow+afterTime[j],true)*afterIntens[j]*Q;
            }
        }
    }
    double ADC = round(ElectronicNoise(integral,electNoiseSigma));
    if(ADC>32767){
        cerr << "Too high values" << endl;
        cerr << endl;
        return 32767;
    }
    return ADC;
}

int Channel::ShaperSignalADCs(double const& timeWindow, double const& integrationWindow, double const& electNoiseSigma) const {
    /*The result of the discretisized output of the shaper.*/
    double integral(0.0);
    for(unsigned int i=0; i<signal.size();i++){
        //cout << "Pixels = " << signal[i]->getPixels() <<" Q: "<<Q<< endl;
        //cout << "Contribution of this pixel = " << round(integrateCurrent(signal[i]->getTime(),signal[i]->getPixels(),timeWindow,integrationWindow))<< endl;
        integral+=DiscreteOutput(-1*timeWindow+integrationWindow+signal[i]->getTime(),true)*signal[i]->getPixels()*Q;
        for(unsigned int k=0;k<signal[i]->geteNumberOfAfterpulses();k++){
            vector<double> afterTime=signal[i]->getAfterPulses()[k]->getTime();
            vector<double> afterIntens=signal[i]->getAfterPulses()[k]->getIntensity();
            for(unsigned int j=0;j<afterTime.size();j++){
                //integral+=DiscreteOutput(timeWindow-integrationWindow-afterTime[j],true)*afterIntens[j]*Q;
                integral+=DiscreteOutput(-1*timeWindow+integrationWindow+afterTime[j],true)*afterIntens[j]*Q;
            }
        }
    }
    //std::cout<<integral<<std::endl;
    double ADC = round(ElectronicNoise(integral,electNoiseSigma));
    //std::cout<<"ADC :"<<ADC<<std::endl;
    if(ADC>32767){
        cerr << "Too high values in Shaper signal ADCs" << endl;
 std::cout<<"ADC :"<<ADC<<std::endl;
 std::cout<<"integral was: "<<integral<<endl;
        cerr << endl;
        return 32767;
    }
    return ADC;
}


void Channel::addNoiseHit(NoiseHit* noise) {
    hits.push_back(noise);
}

void Channel::addSignalHit(NoiseHit* sign) {
    signal.push_back(sign);
}

NoiseHit* Channel::getNoiseHits(unsigned int const& index) const {
    return hits[index];
}
NoiseHit* Channel::getNoiseHits(unsigned int const& index) {
    return hits[index];
}

void Channel::ResetHits() {
    for(unsigned int i=0; i<hits.size();i++){
        delete hits[i];
    }
    hits.clear();
    for(unsigned int i=0; i<signal.size();i++){
        delete signal[i];
    }
    signal.clear();
}

unsigned int Channel::getHitsNumber() const {
    return hits.size();
}

double Channel::getRiseTime() const {
    return RiseTime;
}

double Channel::ElectronicNoise(double const& mean, double const& sigma) const{
    //std::cout<<mean<<" "<<sigma<<std::endl;
    double Max=RAND_MAX;
    double Range=10*sigma;
    double c=1/(sigma*sqrt(2*3.14159265))*Range;
    double x=(rand()/Max-0.5)*Range+mean;
    double u=rand()/Max;
    while(c*1/Range*u>gaussian(mean,sigma,x)){
        x=(rand()/Max-0.5)*Range+mean;
        u=rand()/Max;
    }
    return x;
}

double Channel::gaussian(double const& mean, double const& sigma, double const& x) const{
    return 1/(sigma*sqrt(2*3.14159265))*exp(-0.5*((x-mean)/sigma)*((x-mean)/sigma));
}


double Channel::integrateCurrent(double const& t0, double const& intens,double const& timeWindow, double const& integrationWindow) const {
    //        / A*t when t in [0, riseTime] 
    // I(t) = |
    //        \ B*exp(-(t-riseTime)/tauIN)+(1-B)*exp(-(t-riseTime)/tauR) when t>riseTime
    // return Integral of I(t) from 0 to t
    //                              Measurement
    // t0                                |<--integration window--->|
    // |_________________________________|_________________________|
    // |<--------------timeWindow--------------------------------->|
    // t0: the time when the noise hit is generated (inside the timeWindow) 
    // the time of the noise hit with respect to the zero of the detectors timeWindow
    // timeWindow: the time window for noise creation
     
    double intermediate(0.0);
    if(t0>timeWindow){
        //noise hit is generated out of the time window at the upper side
        return 0;
    }
    if(timeWindow-integrationWindow<t0){
        //noise hit is generated inside the integration window
        if(timeWindow-t0<RiseTime){
            //integrate the current from 0 to t and t in [0, riseTime]
            intermediate=0.5*Q*getTauQ()/(getTauIn()*getTauR())*(timeWindow-t0)*(timeWindow-t0)/RiseTime;
        } else {
            //integrate the current from 0 to t and t in [riseTime,timeWindow]
            intermediate=0.5*Q*getTauQ()/(getTauIn()*getTauR())*(RiseTime);
            intermediate+=Q*((getTauQ()-getTauR())*exp(-(timeWindow-t0-RiseTime)/getTauR())+(getTauIn()-getTauQ())*
                    exp(-(timeWindow-t0-RiseTime)/getTauIn()))/(getTauR()-getTauIn())+Q;
        }
    } else if(timeWindow-integrationWindow<t0+RiseTime) {
        //noise hit is generated out of the integration window at the lower side but t+riseTime is inside the integration window
        //the maximum is at t0+riseTime
        //integrate the current from timeWindow-integration window to t0+riseTime and from t0+riseTime to timeWindow
        intermediate=0.5*Q*getTauQ()/(getTauIn()*getTauR())*(RiseTime)-0.5*Q*getTauQ()/(getTauIn()*getTauR())*
                (/*RiseTime-*/(timeWindow-integrationWindow-t0)*(timeWindow-integrationWindow-t0)/RiseTime);
        intermediate+=Q*((getTauQ()-getTauR())*exp(-(timeWindow-t0-RiseTime)/getTauR())+(getTauIn()-getTauQ())*
                exp(-(timeWindow-t0-RiseTime)/getTauIn()))/(getTauR()-getTauIn())+Q;
    } else {
        //none of the case above
        //noise hit is generated out of the integration window at the lower side and t+riseTime is also out of the integration window
        //the maxinum of the pulse is out of integration window
        //integrate the current from timeWindow-integration window to timeWindow
        intermediate=Q*((getTauQ()-getTauR())*exp(-(timeWindow-t0-RiseTime)/getTauR())+(getTauIn()-getTauQ())*
                exp(-(timeWindow-t0-RiseTime)/getTauIn()))/(getTauR()-getTauIn())-Q*((getTauQ()-getTauR())*
                        exp(-(timeWindow-integrationWindow-t0-RiseTime)/getTauR())+(getTauIn()-getTauQ())*
                        exp(-(timeWindow-integrationWindow-t0-RiseTime)/getTauIn()))/(getTauR()-getTauIn());
    }
    return (intermediate*intens)/(1.0+0.5*getTauQ()/(getTauIn()*getTauR())*(RiseTime));
}

int Channel::round(double const& x) const {
    double t=x-floor(x);
    if (t>=0.5) {
        return ceil(x);
    }
    else {
        return floor(x);
    }
}
