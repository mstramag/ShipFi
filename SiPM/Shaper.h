/*
 * Shaper.h
 *
 *  Created on: Jan 8, 2013
 *      Author: bruggiss
 */

#ifndef SHAPER_H_
#define SHAPER_H_

#include <vector>
#include <string>

using namespace std;

class Shaper {
public:
    Shaper(double const& tC, double const& tS, unsigned int const& n);
    virtual ~Shaper();

    virtual double OutFunction(double const& tauQ, double const& tauIn, double const& tauR, double const& riseTime, double const& t) const=0;
    virtual double OutputForStandardInput(double const& t,bool const& normalized) const=0;

    void initialize(double const& rt, double const& rs,double const& rq,
            double const& cg,double const& ceq,double const& cd,double const& cq,unsigned int const& steps, double const& maxTime);

    double DiscreteOutFunction(double const& t, bool const& normalized) const;

    double getTimeCSA() const;
    double getTimeShape() const;
    double getN() const;
    virtual double geta() const;

    double getMaxStandard() const;
    double getMax() const;
    double getTimeAtMax() const;

    virtual Shaper* clone() const=0;

protected:
    double timeCSA;
    double timeShape;
    unsigned int N;
    vector<double>    Values;
    double max;
    double timeAtMax;
    double maxStandard;
    double maximalTime;
    double dt;

    int round(double const& x) const; //rounds of the double x and returns an int. this is used to convert the integrated current into adc counts.
};



#endif /* SHAPER_H_ */
