/*
 * SignalChannels.cc
 *
 *  Created on: Jan 22, 2013
 *      Author: bruggiss
 */

#include "SignalChannels.h"
#include <ctime>
#include <iostream>
#include "TF1.h"

#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAbsPdf.h"
#include "RooCBShape.h"
#include "TH1.h"
#include "RooArgSet.h"

#include <fstream>
#include <string>
#include "TFile.h"
#include <chrono>
#include "TMath.h"

using namespace std;
//using namespace RooFit; 

SignalChannels::SignalChannels(double const& Width, double const& Hight,double const& epoxy, TRandom3* rand,unsigned int const& numbOfDistr,double QuantumEfficiency)
: random(rand),width(Width),hight(Hight),dx(Width/4.0),dy(Hight/24.0),epoxyThicknes(epoxy),QE(QuantumEfficiency) {

//std::cout<<QE<<std::endl;
  // Constructor of class SignalChannels //
  //Number of pixel in x and y direction !!! Hardcoded !!!
  double npix_y = 26;
  double npix_x = 6400;


  //Initialisation of pixels
  for(unsigned int i=0;i<npix_x;i++) {
    for(unsigned int j=0;j<npix_y;j++) {
      pixels[i][j]=false;
      pix_in_channel[int(floor(i/4.0))]=0;
      photons_in_channel[int(floor(i/4.0))]=0;
    }
  }
  cerr << endl;


  //Display of the process    
  for(unsigned int r=0;r<100;r++){
    cerr << "#";
  }
  cerr << endl;
}

void SignalChannels::Debug(){
int n_channel=1600;
   for(unsigned int m =0;m<n_channel;m++)
  {
 //   if(pix_in_channel[m]!=0)std::cout<<"debug channel: "<<m<<"has pixels: "<<pix_in_channel[m]<<std::endl;
  }


}

void SignalChannels::InitializePhotons(){
double npix_y = 26;
double npix_x = 6400;
//std::cout<<"Initialising PhotonsAndPixels"<<std::endl;
  for(unsigned int i=0;i<npix_x;i++) {
    for(unsigned int j=0;j<npix_y;j++) {
      pixels[i][j]=false;
      photons_in_channel[int(floor(i/4.0))]=0;
    }
  }
}

void SignalChannels::InitializePixels(){
double npix_y = 26;
double npix_x = 6400;
//std::cout<<"Initialising PhotonsAndPixels"<<std::endl;
  for(unsigned int i=0;i<npix_x;i++) {
    for(unsigned int j=0;j<npix_y;j++) {
      pix_in_channel[int(floor(i/4.0))]=0;
    }
  }
}
void SignalChannels::fillPixels(double const& FCx, double const& FCy, unsigned int const& Photons, double const& fiberdiam, unsigned int const& method, double const& angle_cut) {

 //std::cout<<QE<<std::endl; 
  double npix_y = 26;
  double npix_x = 6400;
  double r_ale;
 
  for(unsigned int i=0;i<Photons;i++) {
    r_ale=sqrt(random->Rndm())*((fiberdiam-30.0e-6)/2.0);   // uniform distribution over the fibre cross section (remove cladding 30um)
    double phi_ale=random->Rndm()*2.0*M_PI; // azimuth angle [0-360°]
    //cout <<"phi" << phi_ale<<endl;
    double emission= getRandomAngleOfEmission(angle_cut); // polar emission angle distribution [0-90°]
    
        // photon arrival positon at pixel has 3 contributions
        // 1) FC fibre center 
        // 2) Fibre surface exit position
        // 3) Epoxy propagation displacement z=d, r=d/cos(emission), x=r*sin(emission)*cos(phi)
        
    double xp=FCx+r_ale*cos(phi_ale)+cos(phi_ale)*tan(emission)*epoxyThicknes; 
    double yp=hight/2.0+FCy+r_ale*sin(phi_ale)+sin(phi_ale)*tan(emission)*epoxyThicknes; 
    bool y_pos=false;

        // check if y position hits the active area
        
    int py=int(yp/dy);  
    if (py>=0 && py<npix_y){
      double Y_i=(py+0)*dy+5e-6;    // start active area y
      double Y_f=(py+1)*dy-5e-6;    // end active area y
      if( (yp-Y_i)>0 && (Y_f-yp>0)) {y_pos=true;}
    }    
          // check if x position hits the active area

    int ch_x=int(xp/250e-6); // channel number
    int px=-1; // pixel number
    double in_ch_x = xp-ch_x*250e-6; // inter channel postion
    //std::cout<<in_ch_x<<" "<<ch_x<<std::endl;
          // Set pixel row in channel note this is H2014 definition

    if (in_ch_x>15e-6 && in_ch_x<62.5e-6)  {  px=ch_x*4+0;}      
    else if (in_ch_x>72.5e-6 && in_ch_x<120e-6)  {  px=ch_x*4+1;}      
    else if (in_ch_x>130e-6 && in_ch_x<177.5e-6)  {  px=ch_x*4+2;}      
    else if (in_ch_x>187.5e-6 && in_ch_x<235e-6)  {  px=ch_x*4+3;}      
    //else{std::cout<<"coucou"<<std::endl;}
    if (px<npix_x && px>=0 && y_pos) {
      //std::cout<<"coucou "<<px<<" "<<py<<" "<<py<<std::endl;
      double rn=random->Rndm();
      if(rn>0.39) continue;
//      std::cout<<"px , py: "<<px<<" , "<<py<<std::endl;
      pixels[px][py]=true;
      photons_in_channel[int(floor(px/4.0))]+=1;
      //std::cout<<"photons in channel "<<int(floor(px/4.0))<<" plus1"<<std::endl;
      TPixels.push_back(new TBox(floor(xp/dx)*dx,floor((yp-hight/2.0)/dy)*dy,ceil(xp/dx)*dx,ceil((yp-hight/2.0)/dy)*dy));
    }
  }
}


// Fills the channels with the fired pixel from previous part 

void SignalChannels::fillChannels() {
  double npix_y = 26;
  double npix_x = 6400;
  int n_channel =1600;

  // Fills the channels with the fired pixel from previous part 

  for(unsigned int j=0;j<npix_x;j++) {
    for(unsigned int k=0;k<npix_y;k++) {
      if(pixels[j][k]) {
        //std::cout<<"px , py: "<<j<<" , "<<k<<std::endl;
        pix_in_channel[int(floor(j/4.0))]+=1;
        //std::cout<<"channel recognised: "<<floor(j/4.0)<<std::endl;
      }
    }
  }
   for(unsigned int m =0;m<n_channel;m++)
  {
//    if(pix_in_channel[m]!=0)std::cout<<"channel: "<<m<<"has pixels: "<<pix_in_channel[m]<<std::endl;
  }
 //Cross-talk between channels, 5% >0.5 pe for neighbooring channel, 1% >0.5 pe for next neighbooring channel

  for(unsigned int l = 0; l<n_channel;l++){
    double corr=0.0;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator (seed);
    if(l==0){
      std::poisson_distribution<int> distribution1 (pix_in_channel[1]*0.05);
      std::poisson_distribution<int> distribution2 (pix_in_channel[2]*0.01);
      corr = distribution1(generator)+distribution2(generator);
      corr_XT.push_back(corr);
    }
    else if(l==1){
      std::poisson_distribution<int> distribution1 (pix_in_channel[0]*0.05);
      std::poisson_distribution<int> distribution2 (pix_in_channel[2]*0.05);
      std::poisson_distribution<int> distribution3 (pix_in_channel[3]*0.01);
      corr = distribution1(generator)+distribution2(generator)+distribution3(generator);
      corr_XT.push_back(corr);
    }
    else if(l==n_channel-2){
    std::poisson_distribution<int> distribution1 (pix_in_channel[n_channel-3]*0.05);
      std::poisson_distribution<int> distribution2 (pix_in_channel[n_channel-4]*0.01);
      std::poisson_distribution<int> distribution3 (pix_in_channel[n_channel-1]*0.05);
      corr = distribution1(generator)+distribution2(generator)+distribution3(generator);
      corr_XT.push_back(corr);
    }
    else if(l==n_channel-1){
      std::poisson_distribution<int> distribution1 (pix_in_channel[n_channel-2]*0.05);
      std::poisson_distribution<int> distribution2 (pix_in_channel[n_channel-3]*0.01);
      corr = distribution1(generator)+distribution2(generator);
      corr_XT.push_back(corr);
    }
    else {
      std::poisson_distribution<int> distribution1 (pix_in_channel[l-2]*0.01);
      std::poisson_distribution<int> distribution2 (pix_in_channel[l-1]*0.05);
      std::poisson_distribution<int> distribution3 (pix_in_channel[l+1]*0.05);
      std::poisson_distribution<int> distribution4 (pix_in_channel[l+1]*0.01);
      corr = distribution1(generator)+distribution2(generator)+distribution3(generator)+distribution4(generator);
      corr_XT.push_back(corr);
    }
  }
  
  for(unsigned int m =0;m<n_channel;m++)
  {
    pix_in_channel[m]=pix_in_channel[m]+corr_XT[m];
//    if(pix_in_channel[m]!=0)std::cout<<"channel: "<<m<<"has pixels: "<<pix_in_channel[m]<<std::endl;
  }
}

// Gives the number of pixel in a channel

unsigned int SignalChannels::PixInChannel(unsigned int const& index) const {
    return pix_in_channel[index];
}

// Reset de number of photon in each pixels
void SignalChannels::Reset() {

  double npix_y = 26;
  double npix_x = 6400;

  for(unsigned int i=0;i<npix_x;i++) {
    for(unsigned int j=0;j<npix_y;j++) {
      pixels[i][j]=false;
      pix_in_channel[int(floor(i/4.0))]=0;
      photons_in_channel[int(floor(i/4.0))]=0;
    }
  }

  for(unsigned int j=0;j<TPixels.size();j++){
    delete(TPixels[j]);
  }
  TPixels.clear();
}

//Get width from channels
double SignalChannels::getWidth() const {
    return width;
}

//Get hight from channels
double SignalChannels::getHight() const {
    return hight;
}

vector<TBox*> SignalChannels::getTPixels() const {
    return TPixels;
}


//Angular distribution from photon at the fibre end
//Distribution follow Simon Nieswand measurement from his master project for a single cladded fibre
//Approximation -> Linear between 0 and 32°, quadratic from 32° and 90°
/* OLD VERSION NOT WORKING
Double_t SignalChannels::distributionOfTheAngle(Double_t* x, Double_t* p) const {
  if(*x<0.0) {
    return 0.0;
  }
  //if(*x<32*(2.0*3.1415)/360.0) { 
    //return 0.0+0.00203*(*x);
  if(*x<32*(2.0*3.1415)/360.0){
    return 0.0+0.03125*(*x);
  }

  if (32*(2.0*3.1415)/360.0<*x<46*(2.0*3.1415)/360.0){
    return 0.95;
  }

  else{
    return 1.565e-1 -3.478e-3*(*x)+1.932e-5*(*x)*(*x);
    //return 5.863636 -0.1503788*(*x)+9.46969e-4*(*x)*(*x);
  }


}
*/
 Double_t SignalChannels::distributionOfTheAngle(Double_t* x, Double_t* p) const {
  double prob=0.;

  if(*x<0.0) {
    prob =0.0;
  }
  else if(*x<34){
    prob= 0.0+0.025*(*x);
  }
  else if(*x>34 && *x<34){
    prob= 1;
  }
  else{
    prob =  0.0150496 + 34.3143*TMath::Exp(-*x* 0.109388);
  }
return prob;
}
//Generate photon emission angle at the fibre end

double SignalChannels::getRandomAngleOfEmission(double const& angle_cut) const {  
  //Emission photon are cut after 40° -> Hypthosis that photon with larger angle than 40° will be attenuated in
  // the fibres
  TF1* func = new TF1("Func",this,&SignalChannels::distributionOfTheAngle,0,angle_cut,0); 
  double alpha1=func->GetRandom();
  //cout << "alpha1 before correction"<< alpha1*180/3.1415 <<endl;
  //cout << "alpha1 after correction"<< asin((1.0/1.52)*sin(alpha1*180/3.1415)) <<endl;
  delete(func);

  //Simon measurement are done in air (refraction index 1) and we are interested to see the angle beteween epoxy(refraction index 1.52) and
  //fibre core (refraction index 1.59). So to transform angle from air to core -> 1/1.59 and from core to epoxy 1.59/1.52 
  //The final angle is given by 1/1.52
  return asin((1.0/1.52)*sin(alpha1));    
} 




vector<double> SignalChannels::getRandomPhi() const {
  return randomPhi;
}


unsigned int SignalChannels::PhotInChannel(unsigned int const& index) const {
  return photons_in_channel[index];
}



