/*
 * SignalChannelS.h
 *
 *  Created on: Jan 22, 2013
 *      Author: bruggiss
 */

#ifndef SIGNALCHANNELS_H_
#define SIGNALCHANNELS_H_

#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TBox.h"
#include "TH1F.h"
#include <vector>

struct Position {
    double X;
    double Y;
};

using namespace std;

class SignalChannels {
public:
    SignalChannels(double const& width, double const& hight,double const& epoxy, TRandom3* rand,unsigned int const& numbOfDistr,double QuantumEfficiency);
    void fillPixels(double const& FCx, double const& FCy, unsigned int const& Photons, double const& fiberdiam, unsigned int const& method, double const& angle_cut);
    void fillChannels();
    void InitializePhotons();
    void InitializePixels();
    void Debug();
    void PhotonVector( double const& FCx, double const& FCy, unsigned int const& Photons, double const& fiberdiam, unsigned int const& method);  //prova 13 ott vettore fotoni
   
    unsigned int PixInChannel(unsigned int const& index) const;
    unsigned int PhotInChannel(unsigned int const& index) const;

    void Reset();
    double getWidth() const;
    double getHight() const;
    vector<TBox*> getTPixels() const;
    vector<double> getRandomPhi() const;
    
    
    


private:
    TRandom3* random;
    
    double width;
    double hight;
    double dx;
    double dy;
    double npix_x;
    double npix_y;
    double epoxyThicknes;
    bool pixels[6400][26];
    unsigned int pix_in_channel[1600];
    unsigned int photons_in_channel[1600];
    vector<TBox*> TPixels;
    //double signal_in_channel[1600];
    vector<double> randomPhi;
    vector<double> corr_XT;
    double QE;
    double getRandomAngleOfEmission(double const& angle_cut) const;
    Double_t distributionOfTheAngle(Double_t* x, Double_t* p) const;

    TH1F hist;
};


#endif /* SIGNALCHANNELS_H_ */
