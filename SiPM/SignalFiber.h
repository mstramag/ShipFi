/*
 * SignalFiber.h
 *
 *  Created on: Jan 21, 2013
 *      Author: bruggiss
 */

#ifndef SIGNALFIBER_H_
#define SIGNALFIBER_H_

#include "SignalChannels.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TCanvas.h"

using namespace std;

class SignalFiber {
public:
    SignalFiber();
    SignalFiber(double const& X, double const& Y, double const& posSigma, double const& limit, double const& diam, double const& diamSigma, TRandom3* rand);

    //int getLine() const;
    //int getColumn() const;
    double getX() const;
    double getY() const;
    void createPhotons(double const& theta, double const& phi, double const& x0, unsigned int const& mean, double const& sigma,double excitation_point,double const& fiber_length, double ph_trapped_emi, double mirror_reflectivity, double att_lenght, double fxt);
    unsigned int getNumbPhotons() const;
    unsigned int getNumbUVPhotons() const;
    void setNumbPhotons(unsigned int const& phot);
    void setNumbUVPhotons(unsigned int const& UVphot);
    void addManyPhoton(unsigned int newpho);
    void addPhoton();
    void deletePhoton();
    double getDiameter() const;
    void Check();
    bool Checked() const;
    void UnCheck();

private:
    //int line;
    //int column;
    unsigned int photons;
    unsigned int UVphotons;
    double x;
    double y;
    double diameter;
    
    TRandom3* random;
    bool checked;    //indicates if already checked for crosstalk
    //double p_impact;
    //double corde;
    double getImpactP(double const& theta, double const x0) const;
    double getChord(double const& theta, double const& phi, double const& x0) const;
    int round(double const& x) const;
};


#endif /* SIGNALFIBER_H_ */
