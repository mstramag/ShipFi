/*
 * NoiseHit.h
 *
 *  Created on: Dec 4, 2012
 *      Author: bruggiss
 */

#ifndef NOISEHIT_H_
#define NOISEHIT_H_

#include "AfterPulses.h"
#include <vector>

using namespace std;

/*
 * This class characterizes a noise hit, i.e. a pixel that has fired due to a thermal liberation of an electron and all the pixels that are fired as well due to cross-talk.
 */


class NoiseHit {
public:
    NoiseHit(double const& t=0.0, unsigned int const& p=0);
    ~NoiseHit();

    double getTime() const;
    unsigned int getPixels() const;

    vector<AfterPulses*> getAfterPulses() const; //returns the afterpulses collection

    unsigned int geteNumberOfAfterpulses() const; //returns the size of the  afterpulses collection

    void setTime(double const& t);
    void setPixels(unsigned int const& p,double const& t0, double const& recoveryT,
            double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT); //sets the number of pixels and calls the method generateAfterpulses

    void generateAfterpulses(double const& tOfset, double const& recoveryT,
                double const& prob,double const& weightSl, double const& tauSl, double const& tauFa, double const& maxT); //calls the method generateAfterPulses from the afterPulses class
                                                                                                                        // see there for more information


private:
    double time; //the time of the noise hit with respect to the zero of the detectors timeWindow.
    unsigned int pixels; //how many pixels are fired in this hit.
    vector<AfterPulses*> afterpulses; //a list of all afterpulses attributed to this nois hit.

};


#endif /* NOISEHIT_H_ */
