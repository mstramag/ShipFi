/*
 * SignalSetup.h
 *
 *  Created on: Jan 21, 2013
 *      Author: bruggiss
 */

#ifndef SIGNALSETUP_H_
#define SIGNALSETUP_H_
#include <sstream>
#include "SignalFiber.h"
#include "SignalChannels.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TEllipse.h"
#include "TBox.h"
#include "TLine.h"
#include <vector>

using namespace std;

class SignalSetup {
public:
    SignalSetup(double const& diam, double const& diamSigma, double const& gap,
            double const& posSigma, double const& channelWidth, double const& channelHight, double const& epoxy, 
            TRandom3* rand, unsigned int const& numbOfDistr, double const& QuantEff, unsigned int const& layers, 
            unsigned int const& nfibers, double const& trapped, double const& reflectivity, double const& attenuationlength);
    void SimulateParticle(double const& theta, double const& phi, double const& x0, double const& meanPhotons, double const& sigma, double const& probFiberxtalk, unsigned int const& method,unsigned int const& layers,unsigned int const& nfibers, double const& fiber_lenght,double const& fxt,double const& dfxt, double const& angle_cut);
    unsigned int CheckChannel(unsigned int const& index) const;
    unsigned int CheckPhotonChannel(unsigned int const& index) const;
    void Reset();
    void setRNG(TRandom3* rand);
    SignalChannels* getSigChannels() const;
    vector<TEllipse*> getTFibers() const;
    vector<TBox*> getTChannels() const;
    TLine* getTParticle() const;
    double Poisson( double const& mean, unsigned int const& photon);
    int factorial (int n);
 
private:

    unsigned int layers_;
    unsigned int nfibers_;
    double fiber_diameter; //Mean value of the fiber diameter
    double fiber_gap; //Gap between fibers
    double trapped; 
    double reflectivity;
    double attenuationlength; 


    SignalFiber* fibers [6][1600];
    SignalChannels* sig_channels;
    vector<TEllipse*> TFibers;
    vector<TBox*> TChannels;
    TLine* TParticle;
    TRandom3* random;
};


#endif /* SIGNALSETUP_H_ */
