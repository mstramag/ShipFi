CXX = g++

ROOTCFLAGS    = $(shell root-config --cflags)
ROOTLIBS      = $(shell root-config --libs) 
ROOTGLIBS     = $(shell root-config --glibs)
ROOFITFLAGS   = -lHtml -lThread -lMinuit -lMathCore -lMinuit2 -lRooStats -lRooFit -lRooFitCore -lFoam

CXXFLAGS =-I$(ROOTSYS)/include -ansi -Wall $(ROOTLIBS) $(ROOTGLIBS) 
#-std=c++11

CXXFLAGS     += $(ROOTCFLAGS) $(ROOFITFLAGS)

TEMPLATE = lib
TARGET = SiPM
CONFIG += debug

macx {
    CONFIG += staticlib
}

CREATE_ROOT_DICT_FOR_CLASSES=

SOURCES += AfterPulses.cc \
Detector.cc \
NoiseHit.cc \
ShaperFourier.cc \
SignalChannels.cc \
SignalFiber.cc \
SuperDetector.cc \
Channel.cc \
Shaper.cc \
ShaperStehfest.cc \
SignalSetup.cc

HEADERS += $$CREATE_ROOT_DICT_FOR_CLASSES \
AfterPulses.h \
Detector.h \
NoiseHit.h \
ShaperFourier.h \
SignalChannels.h \
SignalFiber.h \
SuperDetector.h \
Channel.h \
Shaper.h \
ShaperStehfest.h \
SignalSetup.h


DEPENDPATH += ./
INCLUDEPATH += $$DEPENDPATH

include(./SiPM.pri)
include($$system(root-config --incdir)/rootcint.pri)

LIBS += \ 
$(ROOTLIBS) -lHtml -lThread -lMinuit -lMathCore -lMinuit2 -lRooStats -lRooFit -lRooFitCore -lFoam
GLIBS = $(ROOTGLIBS)
