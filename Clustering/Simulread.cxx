#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <TVector.h>
#include <vector>

int Simulread(){

std::ifstream inp0("files.list");
  std::string filename;
  filename.clear();
  inp0>>filename;

int k=0;
  while(!inp0.eof()){

TString filetochange(filename+".outputfile.root");
TString filetoread(filename+".root");

   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TTree 	  *fSetChain;
   Int_t           fSetCurrent; 


   // Declaration of leaf types
   Int_t           Event;
   vector<vector<double> > *L1_S1;
   vector<double>  *x0;
   vector<double>  *theta;
   vector<double>  *phi;
   vector<double>  *ch;

   Double_t        ChargeForOnePulse;
   // List of branches
   TBranch        *b_Event;   //!
   TBranch        *b_L1_S1;   //!
   TBranch        *b_x0;   //!
   TBranch        *b_theta;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_ch;   //!

   TBranch       *b_Q;   //!


   vector<double> L1_S1_tot;


TTree *tree;
TTree *settingstree;
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(filetoread);
if (!f || !f->IsOpen()) {f = new TFile(filetoread);      }
      f->GetObject("SimulData",tree);
      f->GetObject("Settings",settingstree);


      if (!tree) return 0;
      if (!settingstree) return 0;

   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fSetChain = settingstree;
   fSetCurrent = -1;
   fSetChain->SetMakeClass(1);


   fChain->SetBranchAddress("L1_S1", &L1_S1, &b_L1_S1);
   fChain->SetBranchAddress("x0", &x0, &b_x0);
   fChain->SetBranchAddress("theta", &theta, &b_theta);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("ch", &ch, &b_ch);

     fSetChain->SetBranchAddress("ChargeForOnePulse", &ChargeForOnePulse, &b_Q);
     if (fSetChain == 0) return 0;
   Long64_t setnentries = fSetChain->GetEntriesFast();
   Long64_t setnbytes = 0, setnb = 0;

   if (fChain == 0) return 0;
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
Double_t gain=0;
Double_t seedthreshold =2.5;
Double_t neighboursthreshold =1.5;
Double_t sumthreshold =4.0;
   for (Long64_t jentry=0; jentry<setnentries;jentry++) {
      Long64_t ientry;
   if (!fSetChain) ientry= -5;
   ientry = fSetChain->LoadTree(jentry);
   if (ientry < 0) break;
   if (fSetChain->GetTreeNumber() != fSetCurrent) {
      fSetCurrent = fSetChain->GetTreeNumber();
   }
      if (ientry < 0) break;
      setnb = fSetChain->GetEntry(jentry);   setnbytes += setnb;
gain=ChargeForOnePulse;
}
   TFile output(filetochange, "RECREATE");
   Int_t EventNumber;
   vector<double> listposition;
   vector<double> listsizes;
   vector<double> light_yield;

  TTree *t = new TTree("vec","Tree with the positions");
  t->Branch("listofposition",&listposition);
  t->Branch("listsizes",&listsizes);
  t->Branch("EventNumber",&EventNumber);
  t->Branch("LY",&light_yield);

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry;
   if (!fChain) ientry= -5;
   ientry = fChain->LoadTree(jentry);
   if (ientry < 0) break;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
    EventNumber=jentry;
    L1_S1_tot.clear();
    listposition.clear();
    listsizes.clear();
    light_yield.clear();
    for(int j=0;j<1600;j++){
	double sum=0;
    	for(int in=0;in<L1_S1->size();in++){
       		sum+=(L1_S1->at(in)).at(j);}
	L1_S1_tot.push_back(sum);
    }

 int cursor = 0;
  int secondaryCursor=0;

  while(cursor< 1600) {
  double seedweight=0;
  std::vector<int> neighboorinfweight;
  std::vector<int> neighboorsupweight;
  int positionseed=0;
  std::vector<int> positionneighboorinf;
  std::vector<int> positionneighboorsup;

  positionneighboorinf.clear();

  positionneighboorsup.clear();

  double clusterposition=0;

   Short_t pix = L1_S1_tot[cursor]/gain;
    int sizecounter=0;
   Double_t total=0;
    if (pix >= seedthreshold) {
           sizecounter++;
           total+=pix;
	   seedweight=pix;
	   positionseed=cursor;
      Short_t pixNeighbor;
      secondaryCursor = cursor - 1;
      while(secondaryCursor >= 0) {
        pixNeighbor = L1_S1_tot[secondaryCursor]/gain;
        if (pixNeighbor >= neighboursthreshold){
          sizecounter++;
           total+=pixNeighbor;
	   neighboorinfweight.push_back(pixNeighbor);
	   positionneighboorinf.push_back(secondaryCursor);
	   
         --secondaryCursor;}
         else break;}

      secondaryCursor = cursor + 1;
      int sup=0;
      while( secondaryCursor < 1600) {
        pixNeighbor = L1_S1_tot[secondaryCursor]/gain;
      if( pixNeighbor >= neighboursthreshold){
          sizecounter++;
           total+=pixNeighbor;
	   neighboorsupweight.push_back(pixNeighbor);
	   positionneighboorsup.push_back(secondaryCursor);
	   ++sup;
	  
      ++secondaryCursor;}
      else break;}
      }
      if (total >= sumthreshold) {   
      light_yield.push_back(total);
      listsizes.push_back(sizecounter);    
      std::vector<double> pinf;
      pinf.clear();
          
      for(UInt_t i=0;i<positionneighboorinf.size();i++) pinf.push_back(positionneighboorinf.at(i));

      std::vector<double> winf;
      winf.clear();
                                                                   
      for(UInt_t i=0;i<neighboorinfweight.size();i++) winf.push_back(neighboorinfweight.at(i));

                                                                   
      std::vector<double> psup;
      psup.clear();
      
      for(UInt_t i=0;i<positionneighboorsup.size();i++) psup.push_back(positionneighboorsup.at(i));
                                                                                                                                
      std::vector<double> wsup;
      wsup.clear();
                                                                                                                                
      for(UInt_t i=0;i<neighboorsupweight.size();i++) wsup.push_back(neighboorsupweight.at(i));


      clusterposition=positionseed*seedweight;
      for(UInt_t i=0;i<positionneighboorinf.size();i++)clusterposition+=pinf.at(i)*winf.at(i);
      for(UInt_t i=0;i<positionneighboorsup.size();i++)clusterposition+=psup.at(i)*wsup.at(i);
      double denom=seedweight;
      for(UInt_t i=0;i<positionneighboorinf.size();i++)denom+=winf.at(i);
      for(UInt_t i=0;i<positionneighboorsup.size();i++)denom+=wsup.at(i);
      clusterposition=clusterposition/denom;
          
      listposition.push_back(clusterposition);
  
    cursor = secondaryCursor + 1; }
     else {
      ++cursor;
  }
}    
t->Fill();

}       
	t->Write();
        //output.Write();
        output.Close();

  inp0>>filename;
k++;
}

return 1;
}
