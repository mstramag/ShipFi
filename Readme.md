# ShipFi
======

## Content of the folder:  
1.SiPM: package containing the simulation core  
2.ShipClass: example of a class to use the simulation  
3.spectrohits_EPFL.root: example of input ntuple  
4.Clustering: folder dedicated to clustering  
    &nbsp;&nbsp;&nbsp;&nbsp;4.1.SimulationT3.root: output of the simulation for station T3  
    &nbsp;&nbsp;&nbsp;&nbsp;4.2.SimulationT4.root: output of the simulation for station T4  
    &nbsp;&nbsp;&nbsp;&nbsp;4.3.Simulread.cxx: macro to perform the clustering  
    &nbsp;&nbsp;&nbsp;&nbsp;4.4.SimulationT3.output.root: output of the clustering for station T3  
    &nbsp;&nbsp;&nbsp;&nbsp;4.5.SimulationT4.output.root: output of the clustering for station T4  
    &nbsp;&nbsp;&nbsp;&nbsp;4.6.files.list: list of the files to run on the clustering  

## Procedure to run the example:
1.Compile the simulation package

```
cd ShipFi/SiPM
qmake
make
cd ..
```

2.Run the example class
```
root -l
root [0] gSystem->Load("./SiPM/libSiPM.so")
root [1] .L ShipClass.C++
root [2] ShipClass cla
root [3] cla.Loop("T3")
root [4] cla.Loop("T4")
root [5] .q
```
3.Run the clustering macro
```
cd Clustering
root -l
root [1] .x Simulread.cxx()
root [2] .q
```

Qt libraries are necessary, created and tested with qt 4.8.4
The program ROOT is necessary, created and tested with root 5.34: -> root.cern.ch
